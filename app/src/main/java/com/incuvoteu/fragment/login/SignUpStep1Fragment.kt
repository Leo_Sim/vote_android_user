package com.incuvoteu.fragment.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.incuvoteu.R
import com.incuvoteu.databinding.FragmentSignUpStep1Binding
import com.incuvoteu.model.User
import com.incuvoteu.ui.login.SignUpActivity
import com.incuvoteu.ui.login.viewmodel.SignUpFragmentStep1ViewModel

class SignUpStep1Fragment : Fragment(){
    private lateinit var binding : FragmentSignUpStep1Binding
    private lateinit var viewmodel : SignUpFragmentStep1ViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_sign_up_step_1,container,false)
        viewmodel = ViewModelProviders.of(this).get(SignUpFragmentStep1ViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.emailCheckBtn.setOnClickListener {
            viewmodel.emailCheck()
        }
        binding.nextBtn.setOnClickListener {
            val parentActivity = context
            if(parentActivity is SignUpActivity){
                parentActivity.nextStep()
            }
        }
    }

    fun getSignupData() : User = User(viewmodel.emailText.value!!, viewmodel.pwdText.value!!, viewmodel.rePwdText.value!!,"01012345678")
}