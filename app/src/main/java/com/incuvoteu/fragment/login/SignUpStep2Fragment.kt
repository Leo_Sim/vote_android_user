package com.incuvoteu.fragment.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.incuvoteu.R
import com.incuvoteu.databinding.FragmentSignUpStep2Binding
import com.incuvoteu.ui.login.SignUpActivity
import com.incuvoteu.ui.login.viewmodel.SignUpFragmentStep2ViewModel

class  SignUpStep2Fragment : Fragment(){
    private lateinit var binding : FragmentSignUpStep2Binding
    private lateinit var viewmodel : SignUpFragmentStep2ViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_sign_up_step_2,container,false)
        viewmodel = ViewModelProviders.of(this).get(SignUpFragmentStep2ViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.nextBtn.setOnClickListener {
            val parentActivity = context
            if(parentActivity is SignUpActivity){
                parentActivity.nextStep()
            }
        }
    }
}