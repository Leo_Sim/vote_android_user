package com.incuvoteu.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.incuvoteu.R
import com.incuvoteu.adapter.CompleteRecyclerAdapter
import com.incuvoteu.adapter.CompleteVoteRecyclerAdapter

import com.incuvoteu.databinding.FragmentVoteOngoingBinding

import com.incuvoteu.paginator.Paginator
import com.incuvoteu.ui.main.viewmodel.OnGoingViewModel
import kotlinx.android.synthetic.main.fragment_vote_ongoing.*


class VoteOngoingFragment : Fragment() {
    private lateinit var binding : FragmentVoteOngoingBinding
    private lateinit var viewmodel : OnGoingViewModel
    private lateinit var paginator : Paginator

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_vote_ongoing,container,false)
        viewmodel = ViewModelProviders.of(this).get(OnGoingViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val adapter = CompleteVoteRecyclerAdapter()
        val lm = LinearLayoutManager(context)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = lm
        paginator = Paginator(
            recyclerView,
            loadMore = { size,page -> loadMore(size,page) },
            fetchState = {viewmodel.fetchState})
    }

    override fun onResume() {
        super.onResume()
        paginator.refresh()
    }

    private fun loadMore(page : Int, size : Int){
        viewmodel.loadMore(page,size)
    }
}