package com.incuvoteu.fragment.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.incuvoteu.R
import com.incuvoteu.adapter.CompleteRecyclerAdapter
import com.incuvoteu.adapter.CompleteVoteRecyclerAdapter
import com.incuvoteu.databinding.FragmentMainHomeBinding
import com.incuvoteu.dummy.DummyGenerator
import com.incuvoteu.model.VoteHeaderItem
import com.incuvoteu.paginator.Paginator
import com.incuvoteu.ui.main.viewmodel.MainHomeFragmentViewModel
import kotlinx.android.synthetic.main.fragment_main_home.*

class MainHomeFragment : Fragment(){
    private lateinit var binding : FragmentMainHomeBinding
    private lateinit var viewmodel : MainHomeFragmentViewModel
    private lateinit var paginator : Paginator

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("devsim","asdfasdf22222")
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_main_home,container,false)
        viewmodel = ViewModelProviders.of(this).get(MainHomeFragmentViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var covers = mutableListOf<Any>(VoteHeaderItem(3,2,1))
        covers.addAll(DummyGenerator.createVoteItem(20))

//        val adapter = MainVoteListAdapter()
//        adapter.addItems(covers)
//        recyclerView.layoutManager = LinearLayoutManager(context)
//        recyclerView.adapter = adapter

        val adapter = CompleteVoteRecyclerAdapter()
        val lm = LinearLayoutManager(context)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = lm
        paginator = Paginator(
            recyclerView,
            loadMore = { size,page -> loadMore(size,page) },
            fetchState = {viewmodel.fetchState})
        Log.d("devsim","asdfasdf")
    }

    override fun onResume() {
        super.onResume()
        Log.d("devsim","resume23")
        paginator.refresh()
    }

    private fun loadMore(page : Int, size : Int){
        viewmodel.loadMore(page,size)
    }
}