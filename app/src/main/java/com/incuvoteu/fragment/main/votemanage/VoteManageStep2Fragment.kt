package com.incuvoteu.fragment.main.votemanage

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.incuvoteu.R
import com.incuvoteu.adapter.VoteManageListAdapter
import com.incuvoteu.databinding.FragmentVoteManageStep2Binding
import com.incuvoteu.fragment.main.MainVoteCreateFragment
import com.incuvoteu.model.ReceivedUserVoteItem
import com.incuvoteu.model.ReceivedVoteOptionItem
import com.incuvoteu.ui.main.viewmodel.votemanage.VoteManageFragmentStep2ViewModel
import kotlinx.android.synthetic.main.fragment_vote_manage_step_2.*
import kotlinx.android.synthetic.main.top_toolbar.*

/**
 * VoteCreate, VoteManage 에서 공통으로 사용됨.
 */
class VoteManageStep2Fragment : Fragment() {
    private lateinit var binding: FragmentVoteManageStep2Binding
    private lateinit var viewmodel: VoteManageFragmentStep2ViewModel

    private lateinit var adapter: VoteManageListAdapter
    private var isEdit = false
    private var editItem: List<ReceivedVoteOptionItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val arg = arguments
        if (arg != null) {
            editItem = arg.getParcelable<ReceivedUserVoteItem>("Step2")!!.voteOptions
            Log.d("devsim","pw Size : " + editItem!!.size)
            if (editItem != null) {
                isEdit = true
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_vote_manage_step_2,
            container,
            false
        )
        viewmodel = ViewModelProviders.of(this).get(VoteManageFragmentStep2ViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        init()


        adapter = VoteManageListAdapter(viewmodel, binding.lifecycleOwner!!)
        listView.layoutManager = LinearLayoutManager(context)
        listView.adapter = adapter

        if(isEdit){
            viewmodel.load(editItem!!)
        }

        submitBtn.setOnClickListener {
            if (parentFragment is MainVoteCreateFragment) {
                val p = parentFragment as MainVoteCreateFragment
                p.combineResult()
                submitBtn.isEnabled = false
                submitLoadingBar.visibility = View.VISIBLE
            }
        }

        toolbarBackBtn.setOnClickListener {
            if (parentFragment is MainVoteCreateFragment) {
                val p = parentFragment as MainVoteCreateFragment
                p.backPage()
            }
        }
    }

    private fun init(){
        if (!isEdit) {
            toolbarTtitle.text = " 투표 생성"
            submitBtn.text = "투표 생성"
        }
        else {
            toolbarTtitle.text = "투표 관리"
            submitBtn.text = "투표 수정"
        }
    }

    fun getVoteData() = viewmodel.voteItems.value
    fun enableSubmitBtn(){
        submitBtn.isEnabled = true
        submitLoadingBar.visibility = View.GONE
    }

    fun clear(){
//        viewmodel.clear()
        enableSubmitBtn()
    }
}