package com.incuvoteu.fragment.main.votemanage

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.incuvoteu.R
import com.incuvoteu.databinding.FragmentVoteManageStep1Binding
import com.incuvoteu.fragment.main.MainVoteCreateFragment
import com.incuvoteu.model.ReceivedUserVoteItem
import com.incuvoteu.model.VoteItem
import com.incuvoteu.ui.main.viewmodel.votemanage.VoteManageFragmentStep1ViewModel
import kotlinx.android.synthetic.main.fragment_vote_manage_step_1.*
import java.util.*

/**
 * VoteCreate, VoteManage 에서 공통으로 사용됨.
 */
class VoteManageStep1Fragment : Fragment() {
    private lateinit var binding: FragmentVoteManageStep1Binding
    private lateinit var viewmodel: VoteManageFragmentStep1ViewModel
    private var isEdit = false
    private var editItem: ReceivedUserVoteItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val arg = arguments
        if (arg != null) {
            editItem = arg.getParcelable<ReceivedUserVoteItem>("Step1")
            if (editItem != null) {
                isEdit = true
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_vote_manage_step_1,
            container,
            false
        )
        viewmodel = ViewModelProviders.of(this).get(VoteManageFragmentStep1ViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if(isEdit){
            viewmodel.load(editItem)
        }

        nextBtn.setOnClickListener {
            if (parentFragment is MainVoteCreateFragment) { //투표 생성시
                (parentFragment as MainVoteCreateFragment).nextPage()
            }
//            if (context is VoteManageEditActivity) { //투표 수정시
//                (context as VoteManageEditActivity).nextPage()
//            }
        }

        binding.startDateTxt.setOnClickListener {
            val dialog = DatePickerDialog(
                context!!,
                DatePickerDialog.OnDateSetListener { datePicker, year, month, date ->
                    val newCal = Calendar.getInstance()
                    newCal.set(year, month, date)
                    viewmodel.startDate.value = newCal
                },
                viewmodel.getStartDate(Calendar.YEAR),
                viewmodel.getStartDate(Calendar.MONTH),
                viewmodel.getStartDate(Calendar.DATE)
            )
            dialog.show()
        }

        binding.endDateTxt.setOnClickListener {
            val dialog = DatePickerDialog(
                context!!,
                DatePickerDialog.OnDateSetListener { datePicker, year, month, date ->
                    val newCal = Calendar.getInstance()
                    newCal.set(year, month, date)
                    viewmodel.endDate.value = newCal
                },
                viewmodel.getEndDate(Calendar.YEAR),
                viewmodel.getEndDate(Calendar.MONTH),
                viewmodel.getEndDate(Calendar.DATE)
            )

            dialog.show()
        }


        binding.checkBtn.setOnClickListener {
//            Log.d("devsim", "title : " + viewmodel.title.value)
//            Log.d("devsim", "desc : " + viewmodel.desc.value)
//            Log.d("devsim", "startDate : " + viewmodel.startDateStr.value)
//            Log.d("devsim", "endDate : " + viewmodel.endDateStr.value)
//            Log.d("devsim", "ispaid : " + viewmodel.isPaid.value)
//            Log.d("devsim", "isMultipleChoice : " + viewmodel.isMultipleChoice.value)
//            Log.d("devsim", "isMultiple : " + viewmodel.isMultiple.value)
//            Log.d("devsim", "isSecret : " + viewmodel.isSecret.value)
//            Log.d("devsim", "isHide : " + viewmodel.isHide.value)
        }
    }

    fun getVoteData() = VoteItem(
        viewmodel.id,
        viewmodel.title.value,
        viewmodel.desc.value,
        viewmodel.startDate.value?.timeInMillis,
        viewmodel.endDate.value?.timeInMillis,
        viewmodel.isPaid.value!!,
        viewmodel.isMultipleChoice.value!!,
        viewmodel.isMultiple.value!!,
        viewmodel.isSecret.value!!,
        viewmodel.isOpen.value!!
    )

    fun clear(){
//        viewmodel.clear()
    }
}