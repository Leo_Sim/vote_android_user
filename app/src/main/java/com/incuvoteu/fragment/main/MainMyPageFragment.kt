package com.incuvoteu.fragment.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.incuvoteu.model.NavMenuItem
import com.incuvoteu.R
import com.incuvoteu.adapter.base.SingleBindingRecyclerAdapter
import com.incuvoteu.databinding.FragmentMainMypageBinding
import com.incuvoteu.ui.inquiry.InquiryActivity
import com.incuvoteu.ui.main.MyAccountManageActivity
import com.incuvoteu.ui.main.viewmodel.MainMyPageFragmentViewModel
import com.incuvoteu.ui.notice.NoticeActivity
import com.incuvoteu.ui.setting.SettingsActivity
import com.incuvoteu.viewholder.MenuViewHolder
import kotlinx.android.synthetic.main.fragment_main_mypage.*

class MainMyPageFragment : Fragment() {
    private lateinit var binding: FragmentMainMypageBinding
    private lateinit var viewmodel: MainMyPageFragmentViewModel

    private val menuItems = arrayListOf(
        NavMenuItem(
            "내정보관리",
            R.drawable.auth_black_48,
            MyAccountManageActivity::class.java
        ),
        NavMenuItem(
            "공지사항",
            R.drawable.history_black_48,
            NoticeActivity::class.java
        ),
        NavMenuItem(
            "이벤트",
            R.drawable.privacy_black_48,
            NoticeActivity::class.java
        ),
        NavMenuItem(
            "문의하기",
            R.drawable.secession_black_48,
            InquiryActivity::class.java
        ),
        NavMenuItem(
            "설정",
            R.drawable.settings_black_48,
            SettingsActivity::class.java
        )
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_mypage, container, false)
        viewmodel = ViewModelProviders.of(this).get(MainMyPageFragmentViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val menuAdapter = SingleBindingRecyclerAdapter(R.layout.mypage_menu_item, MenuViewHolder::class)
        menuAdapter.addItems(menuItems)
        profileUserMenuList.layoutManager = GridLayoutManager(context,3)
        profileUserMenuList.adapter = menuAdapter

    }

    override fun onResume() {
        super.onResume()
        loadUserInfo()
    }


    private fun loadUserInfo(){
        viewmodel.loadUserInfo()
    }
}