package com.incuvoteu.fragment.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.incuvoteu.model.NavMenuItem
import com.incuvoteu.R
import com.incuvoteu.adapter.base.SingleBindingRecyclerAdapter
import com.incuvoteu.databinding.FragmentMainMypageBinding
import com.incuvoteu.ui.inquiry.InquiryActivity
import com.incuvoteu.ui.main.MyAccountManageActivity
import com.incuvoteu.ui.main.viewmodel.MainMyPageFragmentViewModel
import com.incuvoteu.ui.notice.NoticeActivity
import com.incuvoteu.ui.setting.SettingsActivity
import com.incuvoteu.viewholder.MenuViewHolder
import kotlinx.android.synthetic.main.fragment_main_mypage.*

class MainStoreFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main_store,container,false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }
}