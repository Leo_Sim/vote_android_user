package com.incuvoteu.fragment.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import com.incuvoteu.R
import com.incuvoteu.adapter.base.BasePagerAdapter
import com.incuvoteu.databinding.FragmentMainVoteBinding
import com.incuvoteu.fragment.VoteCompleteFragment
import com.incuvoteu.fragment.VoteOngoingFragment
import com.incuvoteu.ui.main.viewmodel.MainVoteFragmentViewModel
import kotlinx.android.synthetic.main.fragment_main_vote.*

/**
 * 투표관리
 * Pager가 투표진행중, 투표마감 Fragment를 가지고있다.
 */
class MainVoteFragment : Fragment(){
    private lateinit var binding : FragmentMainVoteBinding
    private lateinit var viewmodel : MainVoteFragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_main_vote,container,false)
        viewmodel = ViewModelProviders.of(this).get(MainVoteFragmentViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        initPager()
    }

    private fun initPager(){
        val pages = arrayListOf(
            Pair("투표진행중", VoteOngoingFragment::class),
            Pair("투표마감", VoteCompleteFragment::class)
        )
        val pagerAdapter = BasePagerAdapter(childFragmentManager,pages)
        pager.adapter = pagerAdapter

        pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.removeAllTabs()
        tabLayout.addTab(tabLayout.newTab().setText("투표진행중"))
        tabLayout.addTab(tabLayout.newTab().setText("투표마감"))

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                pager.setCurrentItem(tab!!.position, false)
            }
        })
    }


}