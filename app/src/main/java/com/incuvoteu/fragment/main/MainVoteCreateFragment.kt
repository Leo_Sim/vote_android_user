package com.incuvoteu.fragment.main

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.incuvoteu.R
import com.incuvoteu.adapter.base.BasePagerAdapter
import com.incuvoteu.adapter.base.DataPagerAdapter
import com.incuvoteu.api.client.FileApiClient
import com.incuvoteu.api.client.VoteApiClient
import com.incuvoteu.databinding.FragmentMainVoteCreateBinding
import com.incuvoteu.fragment.main.votemanage.VoteManageStep1Fragment
import com.incuvoteu.fragment.main.votemanage.VoteManageStep2Fragment
import com.incuvoteu.model.FileImage
import com.incuvoteu.model.ReceivedUserVoteItem
import com.incuvoteu.model.UserVoteItem
import com.incuvoteu.model.VoteItem
import com.incuvoteu.ui.main.MainActivity
import com.incuvoteu.ui.main.viewmodel.MainVoteCreateFragmentViewModel
import com.incuvoteu.util.FileUtil
import com.incuvoteu.util.ToastUtil
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_sign_up.*
import java.io.File
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody

class MainVoteCreateFragment : Fragment() {
    private lateinit var binding: FragmentMainVoteCreateBinding
    private lateinit var viewmodel: MainVoteCreateFragmentViewModel
    private var isEdit = false
    private var editItem: ReceivedUserVoteItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val arg = arguments
        if (arg != null) {
            editItem = arg.getParcelable<ReceivedUserVoteItem>("voteItem")
            if (editItem != null) {
                val zk = editItem!!.voteOptions
                Log.d("devsim", "에딧 실행함 pzz " + zk!!.size)
                isEdit = true
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_main_vote_create, container, false)
        viewmodel = ViewModelProviders.of(this).get(MainVoteCreateFragmentViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.d("devsim", "ac")
        initPager()
    }

    private fun initPager() {
        if (!isEdit) {
            val pages = arrayListOf(
                Pair("Step1", VoteManageStep1Fragment::class),
                Pair("Step2", VoteManageStep2Fragment::class)
            )
            val pagerAdapter = BasePagerAdapter(childFragmentManager, pages)
            pager.adapter = pagerAdapter
        } else {
            val pages = arrayListOf(
                Triple("Step1", VoteManageStep1Fragment::class, editItem),
                Triple("Step2", VoteManageStep2Fragment::class, editItem)
            )
            val pagerAdapter = DataPagerAdapter(childFragmentManager, pages)
            pager.adapter = pagerAdapter
        }
    }

    fun nextPage() {
        pager.setCurrentItem(1, false)
    }

    fun backPage() {
        pager.setCurrentItem(0, false)
    }

    fun combineResult() {
        val list = childFragmentManager.fragments
        val voteItem = (list[0] as VoteManageStep1Fragment).getVoteData()
        val voteOptionItem = (list[1] as VoteManageStep2Fragment).getVoteData()

        val sender = UserVoteItem(
            voteItem.id,
            -1,
            voteItem.title,
            voteItem.desc,
            voteItem.startDate?.div(1000),
            voteItem.endDate?.div(1000),
            VoteItem.convertSwitchIdxToString(VoteItem.ENUM_IS_PAID, voteItem.isPaid),
            VoteItem.convertSwitchIdxToString(VoteItem.ENUM_IS_MULTIPLE, voteItem.isMultiple),
            VoteItem.convertSwitchIdxToString(
                VoteItem.ENUM_IS_MULTIPLE_CHOICE,
                voteItem.isMultipleChoice
            ),
            VoteItem.convertSwitchIdxToString(VoteItem.ENUM_IS_SECRET, voteItem.isSecret),
            VoteItem.convertSwitchIdxToString(VoteItem.ENUM_IS_OPEN, voteItem.isOpen),
            -1, voteOptionItem, "REAL_TIME_VOTE"
        )

        Log.d("devsim", "combined Result : " + sender.toString())


        var oblist = mutableListOf<Single<FileImage>>()
        for (i in voteOptionItem!!) {
            Log.d("devsim", "passed")
            if (i.imgUri == null) continue
            val vz = FileUtil.getPath(context, Uri.parse(i.imgUri))
            if (vz != null) {
                val file = File(FileUtil.getPath(context, Uri.parse(i.imgUri)))

                val fbody = file.asRequestBody("image/*".toMediaTypeOrNull())
                val part = MultipartBody.Part.createFormData("file", file.name, fbody)
                val addOn = FileApiClient.upload(part, "VOTE_OPTION").observeOn(Schedulers.io())
                    .doOnSuccess({
                        Log.d("devsim", "success : " + it.id)
                        i.fileId = it.id!!
                    })
                oblist.add(addOn)
            }
        }

        val ob = Single.concat(oblist)

        if (!isEdit) {
            ob.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { Log.d("devsim", "multi upload success000") },
                    { err -> ToastUtil.make(err.message) },
                    {
                        Log.d("devsim", "multi upload success")
                        Log.d("devsim", "sender : " + sender.voteOptions!!.size)
                        VoteApiClient.create(sender)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                ToastUtil.make("투표 생성 완료!")
                                (activity as MainActivity).goMain()
                                pager.setCurrentItem(0, false)
                                (list[0] as VoteManageStep1Fragment).clear()
                                (list[1] as VoteManageStep2Fragment).clear()
                            },
                                { err ->
                                    ToastUtil.make(err.message)
                                    (list[1] as VoteManageStep2Fragment).enableSubmitBtn()
                                })
                    })
        } else {
            ob.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { Log.d("devsim", "multi upload success000") },
                    { err -> ToastUtil.make(err.message) },
                    {
                        Log.d("devsim", "multi upload success")
                        Log.d("devsim", "sender : " + sender.voteOptions!!.size)
                        VoteApiClient.update(sender)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                ToastUtil.make("투표 수정 완료!")
                                activity!!.finish()
                            },
                                { err ->
                                    ToastUtil.make(err.message)
                                    (list[1] as VoteManageStep2Fragment).enableSubmitBtn()
                                })
                    })
        }
    }
}