package com.incuvoteu.fragment.inquiry

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.incuvoteu.R
import com.incuvoteu.model.NoticeItem

class FragmentInquiry1 : Fragment(){
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       return inflater.inflate(R.layout.inquery_fragment1,container,false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val items = arrayListOf(
            NoticeItem(
                "2019-07-23 더쇼 사전투표 시작",
                resources.getString(R.string.test_notice_msg)
            ),
            NoticeItem(
                "2019-07-08 일시 장애 공지",
                "죄송합니다"
            )

        )

//        val noticeAdapter = SingleBindingRecyclerAdapter(R.layout.notice_list_item, NoticeViewHolder::class)
//        noticeAdapter.additem(items)
//        noticeRecyclerView.layoutManager = LinearLayoutManager(context)
//        noticeRecyclerView.adapter = noticeAdapter
    }
}