package com.incuvoteu.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.incuvoteu.R
import com.incuvoteu.adapter.CompleteResultRecyclerAdapter
import com.incuvoteu.databinding.FragmentVoteCompleteBinding
import com.incuvoteu.databinding.FragmentVoteOngoingBinding
import com.incuvoteu.paginator.Paginator
import com.incuvoteu.ui.main.viewmodel.OnCompleteViewModel
import kotlinx.android.synthetic.main.fragment_main_home.recyclerView

class VoteCompleteFragment : Fragment() {
    private lateinit var binding : FragmentVoteCompleteBinding
    private lateinit var viewmodel : OnCompleteViewModel
    private lateinit var paginator : Paginator

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_vote_complete,container,false)
        viewmodel = ViewModelProviders.of(this).get(OnCompleteViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val adapter = CompleteResultRecyclerAdapter()
        val lm = LinearLayoutManager(context)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = lm
        paginator = Paginator(
            recyclerView,
            loadMore = { size,page -> loadMore(size,page) },
            fetchState = {viewmodel.fetchState})
    }

    override fun onResume() {
        super.onResume()
        paginator.refresh()
    }

    private fun loadMore(page : Int, size : Int){
        viewmodel.loadMore(page,size)
    }
}