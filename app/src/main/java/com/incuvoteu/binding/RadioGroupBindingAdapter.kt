package com.incuvoteu.binding

import android.util.Log
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener

object RadioGroupBindingAdapter {
    @JvmStatic
    @BindingAdapter(value = ["check","checkAttrChanged"], requireAll = false)
    fun bindRadioGroup(group : RadioGroup, pos : Int, changed : InverseBindingListener){
        Log.d("devsim","pppos : " + pos)
        val rb = group.getChildAt(pos)
        if(rb is RadioButton){
            Log.d("devsim","th : " + Thread.currentThread().name)
            group.check(rb.id)
//            rb.isChecked = true
        }

        group.setOnCheckedChangeListener { group, id ->
            changed.onChange()
        }
    }

    @JvmStatic
    @InverseBindingAdapter(attribute = "check", event = "checkAttrChanged")
    fun inverseBindRadioGroup(group: RadioGroup): Int {
        val id = group.checkedRadioButtonId
        return group.indexOfChild(group.findViewById(id))
    }
}