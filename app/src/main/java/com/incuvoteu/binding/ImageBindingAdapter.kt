package com.incuvoteu.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object ImageBindingAdapter {
    @JvmStatic
    @BindingAdapter("img")
    fun bindImgage(imgView: ImageView, url: String?) {
        checkNotNull(url) { return }
        Glide.with(imgView.context)
            .load(url)
            .into(imgView)
    }
}