package com.incuvoteu.binding

import android.util.Log
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.incuvoteu.adapter.CompleteRecyclerAdapter
import com.incuvoteu.adapter.VoteManageListAdapter
import com.incuvoteu.api.FetchState
import com.incuvoteu.model.VoteOptionItem

object RecyclerBindingAdapter {
    @JvmStatic
    @BindingAdapter("items")
    fun bindItems(recyclerView : RecyclerView, items : List<VoteOptionItem>){
        val adapter = recyclerView.adapter as VoteManageListAdapter
        for(i in items) {
            Log.d("devsim","iotems : " + i.title)
            adapter
        }
        adapter.setItem(items)
    }

    @JvmStatic
    @BindingAdapter("pagingItems", "state")
    fun bindItems(recyclerView: RecyclerView, items: List<Any>?, state: FetchState) {
        checkNotNull(items) { return }
        val adapter = recyclerView.adapter as CompleteRecyclerAdapter
        if (state.isOnLast){
            adapter.lastPage = true
            adapter.notifyDataSetChanged()
        }
        adapter.addItems(items)
        state.isOnLoading = false
    }
}