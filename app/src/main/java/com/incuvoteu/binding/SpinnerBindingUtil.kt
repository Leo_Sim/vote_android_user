package com.incuvoteu.binding

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener

object SpinnerBindingUtil {
    @JvmStatic
    @BindingAdapter(value = ["selection","selectionAttrChanged"], requireAll = false)
    fun bindSpinnerData(
        pAppCompatSpinner: Spinner,
        newSelectedValue: String?,
        newTextAttrChanged : InverseBindingListener
    ) {
        pAppCompatSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                newTextAttrChanged.onChange()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        if (newSelectedValue != null) {
            val pos =
                (pAppCompatSpinner.adapter as ArrayAdapter<String>).getPosition(
                    newSelectedValue
                )
            pAppCompatSpinner.setSelection(pos, true)
        }
    }
    @JvmStatic
    @InverseBindingAdapter(attribute = "selection", event = "selectionAttrChanged")
    fun captureSelectedValue(pAppCompatSpinner: Spinner): String {
        return pAppCompatSpinner.selectedItem as String
    }
}
