package com.incuvoteu.manager

import android.os.Handler
import android.os.Looper
import com.incuvoteu.util.PrefUtil
import timber.log.Timber
import java.util.*

object LangManager {

    var locale: Locale
        set(value) = PrefUtil.set(PrefUtil.LANG_LOCALE, value.language)
        get() {
            val value = PrefUtil.get(PrefUtil.LANG_LOCALE, "")
            if (value.equals("")) {
                return Locale.getDefault()
            }
            return Locale(value)
        }

    fun applyLanguage(_locale: Locale): Unit {
        this.locale = _locale
        Timber.d("Set Lang : %s", locale.language)

        Handler(Looper.getMainLooper()).postDelayed({
            for (it in LangBroadcastManager.getLangChangeBroadcast()!!) {
                it.broadcast(locale)
            }
        }, 100)
    }
}