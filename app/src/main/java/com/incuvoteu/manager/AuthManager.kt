package com.incuvoteu.manager

import com.incuvoteu.util.PrefUtil

object AuthManager {
    enum class SOCIAL_TYPE(val value: Int) {
        NONE(0), EMAIL(1), KAKAO(2)
    }

    var type: Int //소셜 인증 방식
        get() = PrefUtil.get(
            PrefUtil.AUTH_TYPE,
            SOCIAL_TYPE.NONE.value
        )
        set(value) = PrefUtil.set(PrefUtil.AUTH_TYPE, value)

    //사용자 닉네임.. Not pref
    var emailToken: String
        get() = PrefUtil.get(PrefUtil.AUTH_EMAIL_TOKEN, "-")
        set(value) = PrefUtil.set(PrefUtil.AUTH_EMAIL_TOKEN, value)

    var kakaoToken: String
        get() = PrefUtil.get(PrefUtil.AUTH_KAKAO_TOKEN, "-")
        set(value) = PrefUtil.set(PrefUtil.AUTH_KAKAO_TOKEN, value)

    fun logout() {
        type = SOCIAL_TYPE.NONE.value
        emailToken = "-"
        kakaoToken = "-"
    }
}