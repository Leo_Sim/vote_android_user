package com.incuvoteu.manager

import android.util.AttributeSet
import android.widget.TextView
import com.incuvoteu.R
import com.incuvoteu.util.LangUtil
import java.util.*

object LangBroadcastManager {
    private val langChangeBroadcasts : MutableSet<OnLangChangeBroadcast> = hashSetOf()

    fun getLangChangeBroadcast(): Set<OnLangChangeBroadcast> {
        return langChangeBroadcasts
    }

    fun addLangChangeBroadcast(broadcast: OnLangChangeBroadcast) {
        langChangeBroadcasts.add(broadcast)
    }

    fun removeLangChangeBroadcast(broadcast: OnLangChangeBroadcast) {
        langChangeBroadcasts.remove(broadcast)
    }

    interface OnLangChangeBroadcast {
        fun broadcast(locale: Locale)
    }

    fun <T : TextView?> buildLangBroadcast(
        view: T,
        attrs: AttributeSet
    ): OnLangChangeBroadcast {
        val attr =
            view!!.context.obtainStyledAttributes(attrs, R.styleable.FlexibleLangButton)
        val textResource = attr.getResourceId(R.styleable.FlexibleLangButton_android_text, 0)
        val listener = object : OnLangChangeBroadcast {
            override fun broadcast(locale: Locale) {
                if(textResource != 0){
                    view.text = LangUtil.getLocaleResourceString(view.context,locale, textResource)
                }
            }
        }
        attr.recycle()
        return listener
    }

}