package com.incuvoteu.manager

import android.net.Uri
import androidx.lifecycle.MutableLiveData

object ImageManager{
    var testUri : Uri? = null
    var lastPickedImg : String? = null
    set(value) {
        field = value
        lastPickedFilename(value)
        lastPickedLiveData?.value = value
    }
    var lastPickedLiveData : MutableLiveData<String>? = null

    var lastPickedImgFileName : String? = null

    fun lastPickedFilename(uri : String?){
        lastPickedImgFileName = uri
    }
}