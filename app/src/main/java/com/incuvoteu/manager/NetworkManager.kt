package com.incuvoteu.manager

import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import androidx.annotation.RequiresApi
import timber.log.Timber

object NetworkManager {
    private var isConnected: Boolean = false
    fun isConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        checkNotNull(cm) {
            return setConnectedState(false)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val net = cm.activeNetwork
            checkNotNull(net) {
                return setConnectedState(
                    false
                )
            }
            val cp = cm.getNetworkCapabilities(net)
            checkNotNull(cp){
                return setConnectedState(
                    false
                )
            }
            if(cp.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)){
                setConnectedState(true)
            } else if(cp.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)){
                setConnectedState(true)
            }
        } else {
            val networkInfo = cm.getActiveNetworkInfo()
            checkNotNull(networkInfo){
                return setConnectedState(
                    false
                )
            }
            setConnectedState(networkInfo.isConnected())
        }
        return getConnectedState()
    }

    fun setConnectedState(state: Boolean): Boolean {
        isConnected = state
        return isConnected
    }

    fun getConnectedState() = isConnected

    private fun getConnectivityManager(context: Context): ConnectivityManager? {
        return context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun registerNetworkStateReceiver(
        context: Context,
        manager: NetworkStateManager,
        callback: NetworkCallback
    ) {
        Timber.d("registerNetworkStateReceiver")
        val cm: ConnectivityManager? =
            getConnectivityManager(context)
        checkNotNull(cm){
            Timber.d("NetworkStateReceiver : Received null ConnectivityManager")
            return
        }
        val builder = NetworkRequest.Builder()
        cm.registerNetworkCallback(builder.build(), callback)
        manager.setReceiver(callback)
    }

    fun registerNetworkStateReceiver(
        context: Context,
        manager: NetworkStateManager,
        callback: BroadcastReceiver
    ) {
        Timber.d("registerNetworkStateReceiverLegacy")
        val intentFilter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        context.registerReceiver(callback, intentFilter)
        manager.setReceiver(callback)
    }

    class NetworkStateManager {
        private var networkCallback : ConnectivityManager.NetworkCallback? = null
        private var legacyNetworkCallback : BroadcastReceiver? = null

        fun setReceiver(_callback : ConnectivityManager.NetworkCallback?){
            this.networkCallback = _callback
        }

        fun setReceiver(_callback : BroadcastReceiver?){
            this.legacyNetworkCallback = _callback
        }

        fun unregisterAllReceivers(context : Context){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                val cm =
                    getConnectivityManager(
                        context
                    )
                checkNotNull(cm){
                    Timber.d("Can't unregister NetworkReceiver : received null ConnectivityManager")
                    return
                }
                if(networkCallback != null) {
                    cm.unregisterNetworkCallback(networkCallback!!)
                }
            } else {
                if(legacyNetworkCallback != null){
                    context.unregisterReceiver(legacyNetworkCallback)
                }
            }
        }


    }
}