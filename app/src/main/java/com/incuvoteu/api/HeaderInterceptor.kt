package com.incuvoteu.api

import com.incuvoteu.manager.AuthManager
import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        if(AuthManager.type == AuthManager.SOCIAL_TYPE.EMAIL.value) {
            request = chain.request()
                .newBuilder()
                .addHeader("X-AUTH-TOKEN", AuthManager.emailToken)
                .build()
        }else if(AuthManager.type == AuthManager.SOCIAL_TYPE.KAKAO.value){
            request = chain.request()
                .newBuilder()
                .addHeader("X-AUTH-TOKEN", AuthManager.kakaoToken)
                .build()
        }
        return chain.proceed(request)
    }
}