package com.incuvoteu.api

data class ApiResponse<T>(
    val code : String,
    val data : T?,
    val msg : String?

)