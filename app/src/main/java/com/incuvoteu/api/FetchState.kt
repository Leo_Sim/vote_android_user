package com.incuvoteu.api

class FetchState (
    var isOnLoading : Boolean = false,
    var isOnError : Boolean = false,
    var isOnLast : Boolean = false
)