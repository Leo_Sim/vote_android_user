package com.incuvoteu.api.client

import com.incuvoteu.api.operator.ApiErrorOperator
import com.incuvoteu.model.ReceivedUser
import com.leo.commonrecyclerpager.api.NetworkModule
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object UserApiClient {
    fun get(): Single<ReceivedUser> {
        return NetworkModule.userService.get()
            .subscribeOn(Schedulers.io())
            .lift(ApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun update(nickName : String): Single<Any> {
        return NetworkModule.userService.update(nickName)
            .subscribeOn(Schedulers.io())
            .lift(ApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }
}