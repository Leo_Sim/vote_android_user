package com.incuvoteu.api.client

import com.incuvoteu.api.operator.ApiErrorOperator
import com.incuvoteu.model.User
import com.leo.commonrecyclerpager.api.NetworkModule
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object SignApiClient {
    fun signup(user: User): Single<Any> {
        return NetworkModule.signService.signup(
            user.email,
            user.password,
            user.rePassword,
            user.mobile
        )
            .subscribeOn(Schedulers.io())
            .lift(ApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun emailCheck(email: String): Single<Any> {
        return NetworkModule.signService.check(email)
            .subscribeOn(Schedulers.io())
            .lift(ApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun signin(provider : String, kakaoToken : String) : Single<String> {
        return NetworkModule.signService.signin("kakao",kakaoToken)
            .subscribeOn(Schedulers.io())
            .lift(ApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }
}