package com.incuvoteu.api.client

import com.incuvoteu.api.operator.ApiErrorOperator
import com.incuvoteu.model.FileImage
import com.leo.commonrecyclerpager.api.NetworkModule
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody

object FileApiClient {
    fun upload(file : MultipartBody.Part, fileCategory: String): Single<FileImage> {
        return NetworkModule.fileService.upload(file, fileCategory)
            .subscribeOn(Schedulers.io())
            .lift(ApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }
}