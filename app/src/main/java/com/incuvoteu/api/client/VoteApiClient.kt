package com.incuvoteu.api.client

import com.incuvoteu.api.operator.ApiErrorOperator
import com.incuvoteu.api.operator.FetchStateApiErrorOperator
import com.incuvoteu.model.ReceivedUserVoteItem
import com.incuvoteu.model.ReceivedWrapper
import com.incuvoteu.model.UserVoteItem
import com.leo.commonrecyclerpager.api.NetworkModule
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object VoteApiClient {
    fun create(userVoteItem : UserVoteItem): Single<Any> {
        return NetworkModule.voteService.create(userVoteItem)
            .subscribeOn(Schedulers.io())
            .lift(ApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun fetchAll(page : Int, size : Int): Single<ReceivedWrapper<ReceivedUserVoteItem>> {
        return NetworkModule.voteService.fetchAll(page,size,"DESC")
            .subscribeOn(Schedulers.io())
            .lift(FetchStateApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun fetch(voteId : Int) : Single<ReceivedUserVoteItem> {
        return NetworkModule.voteService.fetch(voteId)
            .subscribeOn(Schedulers.io())
            .lift(ApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun update(userVoteItem : UserVoteItem): Single<Any> {
        return NetworkModule.voteService.update(userVoteItem)
            .subscribeOn(Schedulers.io())
            .lift(ApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun fetchOpenAll(page : Int, size : Int): Single<ReceivedWrapper<ReceivedUserVoteItem>> {
        return NetworkModule.voteService.fetchOpenAll(page,size,"DESC")
            .subscribeOn(Schedulers.io())
            .lift(FetchStateApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun fetchCloseAll(page : Int, size : Int): Single<ReceivedWrapper<ReceivedUserVoteItem>> {
        return NetworkModule.voteService.fetchCloseAll(page,size,"DESC")
            .subscribeOn(Schedulers.io())
            .lift(FetchStateApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun voting(id : Int, voteOptionId : Int, amount : Int): Single<Any> {
        return NetworkModule.voteService.voting(id,voteOptionId,1)
            .subscribeOn(Schedulers.io())
            .lift(ApiErrorOperator())
            .observeOn(AndroidSchedulers.mainThread())
    }

}