package com.leo.commonrecyclerpager.api

import com.incuvoteu.api.HeaderInterceptor
import com.incuvoteu.api.service.FileService
import com.incuvoteu.api.service.SignService
import com.incuvoteu.api.service.UserService
import com.incuvoteu.api.service.VoteService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object NetworkModule {
    private val BASE_URL = "http://192.168.100.57:8080/"
    private val logger: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {level = HttpLoggingInterceptor.Level.BASIC }
    private val client: OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(logger)
        .addInterceptor(HeaderInterceptor())
        .connectTimeout(3000, TimeUnit.MILLISECONDS)
        .build()
    val retrofit: Retrofit = Retrofit.Builder()
        .client(client)
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    //Services
    val userService : UserService = retrofit.create(UserService::class.java)
    val signService : SignService = retrofit.create(SignService::class.java)
    val voteService : VoteService = retrofit.create(VoteService::class.java)
    val fileService : FileService = retrofit.create(FileService::class.java)

}