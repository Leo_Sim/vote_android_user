package com.incuvoteu.api.operator

import android.util.Log
import com.incuvoteu.api.ApiResponse
import com.incuvoteu.api.FetchState
import com.incuvoteu.api.error.ApiError
import com.incuvoteu.api.error.ApiException
import com.incuvoteu.model.ReceivedWrapper
import com.leo.commonrecyclerpager.api.NetworkModule

import io.reactivex.SingleObserver
import io.reactivex.SingleOperator
import io.reactivex.disposables.Disposable
import retrofit2.Response

class FetchStateApiErrorOperator<T> : SingleOperator<ReceivedWrapper<T>, Response<ApiResponse<ReceivedWrapper<T>>>> {
    override fun apply(observer: SingleObserver<in ReceivedWrapper<T>>): SingleObserver<in Response<ApiResponse<ReceivedWrapper<T>>>> {
        return object : SingleObserver<Response<ApiResponse<ReceivedWrapper<T>>>> {
            override fun onSuccess(res: Response<ApiResponse<ReceivedWrapper<T>>>) {
                if (res.isSuccessful) {
                    val apiResponse = res.body()!!.data
                    val isEnd = apiResponse!!.last
                    Log.d("devsim","fetch state : " + isEnd)
                    if(isEnd) {
                        apiResponse!!.fetchState = FetchState(true, false, true)
                    } else {
                        apiResponse!!.fetchState = FetchState(true, false, false)
                    }
                    observer.onSuccess(apiResponse)
                } else {
                    val converter = NetworkModule.retrofit.responseBodyConverter<ApiError>(
                        ApiError::class.java,
                        ApiError::class.java.annotations
                    )
                    val error = converter.convert(res.errorBody()!!)
                    val errorMsg = error?.message
                    observer.onError(ApiException(errorMsg))
                }
            }

            override fun onSubscribe(d: Disposable) {
                observer.onSubscribe(d)
            }

            override fun onError(e: Throwable) {
                Log.d("devsim","netowrk completed fail")
                observer.onError(ApiException("서버 연결 에러"))
            }
        }
    }
}