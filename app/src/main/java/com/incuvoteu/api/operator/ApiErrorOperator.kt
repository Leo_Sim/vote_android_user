package com.incuvoteu.api.operator

import android.util.Log
import com.incuvoteu.api.ApiResponse
import com.incuvoteu.api.EmptyResponse
import com.incuvoteu.api.error.ApiError
import com.incuvoteu.api.error.ApiException
import com.leo.commonrecyclerpager.api.NetworkModule
import io.reactivex.SingleObserver
import io.reactivex.SingleOperator
import io.reactivex.disposables.Disposable
import retrofit2.Response

class ApiErrorOperator<T> : SingleOperator<T, Response<ApiResponse<T>>> {
    override fun apply(observer: SingleObserver<in T>): SingleObserver<in Response<ApiResponse<T>>> {
        return object : SingleObserver<Response<ApiResponse<T>>> {
            override fun onSuccess(res: Response<ApiResponse<T>>) {
                if (res.isSuccessful) {
                    val data = res.body()!!.data
                    if(data != null) {
                        observer.onSuccess(data!!)
                    }else {
                        observer.onSuccess(EmptyResponse() as T)
                    }
                } else {
                    val converter = NetworkModule.retrofit.responseBodyConverter<ApiError>(
                        ApiError::class.java,
                        ApiError::class.java.annotations
                    )
                    val error = converter.convert(res.errorBody()!!)
                    val errorMsg = error?.message
                    observer.onError(ApiException(errorMsg))
                }
            }

            override fun onSubscribe(d: Disposable) {
                observer.onSubscribe(d)
            }

            override fun onError(e: Throwable) {
                Log.d("devsim","asdf fail : " + e.message)
                observer.onError(ApiException("서버와 통신할 수 없음"))
            }
        }
    }
}