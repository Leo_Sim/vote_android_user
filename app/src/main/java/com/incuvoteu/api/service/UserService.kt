package com.incuvoteu.api.service

import com.incuvoteu.api.ApiResponse
import com.incuvoteu.model.ReceivedUser
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Query

interface UserService {
    @GET("/api/user")
    fun get() : Single<Response<ApiResponse<ReceivedUser>>>

    @PUT("/api/user")
    fun update(@Query("nickName")nickName : String) : Single<Response<ApiResponse<Any>>>
}