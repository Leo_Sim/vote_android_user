package com.incuvoteu.api.service

import com.incuvoteu.api.ApiResponse
import com.incuvoteu.model.FileImage
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface FileService {
    @Multipart
    @POST("/api/file/upload/{fileCategory}")
    fun upload(
        @Part file : MultipartBody.Part,
        @Path("fileCategory") fileCategory : String
        ): Single<Response<ApiResponse<FileImage>>>
}