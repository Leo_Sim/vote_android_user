package com.incuvoteu.api.service

import com.incuvoteu.api.ApiResponse
import com.incuvoteu.model.ReceivedUserVoteItem
import com.incuvoteu.model.ReceivedWrapper
import com.incuvoteu.model.UserVoteItem
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface VoteService {
    @POST("/api/vote/superuser/create")
    fun create(@Body dto : UserVoteItem) : Single<Response<ApiResponse<Any>>>

    @GET("/api/vote/list")
    fun fetchAll(@Query("page") page : Int,
                 @Query("size") size : Int,
                 @Query("direction") direction : String
                 ) : Single<Response<ApiResponse<ReceivedWrapper<ReceivedUserVoteItem>>>>

    @GET("/api/vote/detail")
    fun fetch(@Query("voteId") id : Int) : Single<Response<ApiResponse<ReceivedUserVoteItem>>>

    @POST("/api/vote/superuser/update")
    fun update(@Body dto : UserVoteItem) : Single<Response<ApiResponse<Any>>>

    @GET("/api/vote/list/open")
    fun fetchOpenAll(@Query("page") page : Int,
                 @Query("size") size : Int,
                 @Query("direction") direction : String
    ) : Single<Response<ApiResponse<ReceivedWrapper<ReceivedUserVoteItem>>>>

    @GET("/api/vote/list/close")
    fun fetchCloseAll(@Query("page") page : Int,
                     @Query("size") size : Int,
                     @Query("direction") direction : String
    ) : Single<Response<ApiResponse<ReceivedWrapper<ReceivedUserVoteItem>>>>

    @POST("/api/vote/voting")
    fun voting(@Query("voteId") id : Int,
               @Query("voteOptionId") voteOptionId : Int,
               @Query("amount") amount : Int
               ) : Single<Response<ApiResponse<Any>>>
}