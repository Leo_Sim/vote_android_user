package com.incuvoteu.api.service

import com.incuvoteu.api.ApiResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface SignService {
    /**
     * @resp
     * String : Access Token
     */
    @POST("/api/superuser/signup")
    fun signup(
        @Query("email") email: String,
        @Query("password") password: String,
        @Query("rePassword") rePassword: String,
        @Query("mobile") mobile: String
    ): Single<Response<ApiResponse<Any>>>

    /**
     * 이메일 중복검사
     */
    @GET("/api/superuser/email/check")
    fun check(@Query("email") email : String) : Single<Response<ApiResponse<Any>>>

    @POST("/api/signin/{provider}")
    fun signin(@Path("provider") provider : String, @Query("accessToken") kakaoToken: String) : Single<Response<ApiResponse<String>>>
}