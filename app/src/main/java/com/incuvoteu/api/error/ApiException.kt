package com.incuvoteu.api.error

class ApiException(errorMsg : String?) : RuntimeException(errorMsg)