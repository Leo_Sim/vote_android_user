package com.incuvoteu.api.error

data class ApiError(
    val message : String,
    val status : Int,
    val errors : List<Any>,
    val code : String
)