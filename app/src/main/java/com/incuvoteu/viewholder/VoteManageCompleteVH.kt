package com.incuvoteu.viewholder

import android.content.Intent
import android.view.View
import com.incuvoteu.ui.main.VoteResultActivity

class VoteManageCompleteVH : VoteViewHolder {
    constructor(itemView: View) : super(itemView) {
        itemView.setOnClickListener {
            val intent = Intent(itemView.context, VoteResultActivity::class.java)
            itemView.context.startActivity(intent)
        }
    }

    override fun bind(item: Any?) {
        super.bind(item)
    }
}