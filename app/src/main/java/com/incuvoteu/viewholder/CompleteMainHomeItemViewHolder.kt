package com.leo.kakaocompleterecyclerview.viewholder

import android.content.Intent
import androidx.databinding.ViewDataBinding
import com.incuvoteu.databinding.MainHomeVoteListItemBinding
import com.incuvoteu.model.ReceivedUserVoteItem
import com.incuvoteu.ui.main.VoteResultActivity
import com.incuvoteu.util.DateUtil
import com.incuvoteu.viewholder.BaseViewHolder

class CompleteMainHomeItemViewHolder : BaseViewHolder {
    lateinit var binding: MainHomeVoteListItemBinding

    constructor(_binding: ViewDataBinding) : super(_binding.root) {
        if (_binding is MainHomeVoteListItemBinding) {
            this.binding = _binding
        }
    }

    override fun bind(_item: Any?) {
        _item.let {
            if (_item is ReceivedUserVoteItem) {
                _item.startDateStr = DateUtil.getDate(_item.startDate*1000,"yyyy/MM/dd")!!
                _item.endDateStr = DateUtil.getDate(_item.endDate*1000,"yyyy/MM/dd")!!
                binding.item = _item
                binding.root.setOnClickListener {
                    val intent = Intent(itemView.context, VoteResultActivity::class.java)
                    intent.putExtra("voteId",_item.id)
                    itemView.context.startActivity(intent)
                }
            }
        }
    }
}