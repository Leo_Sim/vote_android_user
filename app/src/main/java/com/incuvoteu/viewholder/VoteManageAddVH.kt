package com.incuvoteu.viewholder

import android.view.View
import android.widget.Button
import androidx.lifecycle.ViewModel
import com.incuvoteu.R
import com.incuvoteu.ui.main.viewmodel.votemanage.VoteManageFragmentStep2ViewModel

class VoteManageAddVH : BaseViewHolder{
    private val addVoteBtn : Button
    constructor(itemView: View, viewmodel : ViewModel) : super(itemView) {
        addVoteBtn = itemView.findViewById(R.id.addVoteBtn)

        if(viewmodel is VoteManageFragmentStep2ViewModel){
            addVoteBtn.setOnClickListener {
                viewmodel.addVoteItem()
            }
        }
    }

    override fun bind(item: Any?) {

    }
}
