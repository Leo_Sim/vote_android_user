package com.incuvoteu.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.incuvoteu.R
import com.incuvoteu.model.NoticeItem

class NoticeViewHolder : BaseViewHolder {
    private val titleText: TextView
    private val msgText: TextView
    private val foldingIcon: ImageView

    constructor(itemView: View) : super(itemView) {
        titleText = itemView.findViewById(R.id.titleText)
        msgText = itemView.findViewById(R.id.msgText)
        foldingIcon = itemView.findViewById(R.id.folding_icon)
    }

    override fun bind(item: Any?) {
        if (item is NoticeItem) {
            if (item?.visible) {
                msgText.visibility = View.VISIBLE
            } else msgText.visibility = View.GONE

            titleText?.text = item.title
            msgText?.text = item.msg

            foldingIcon.setOnClickListener {
                if (item?.visible) {
                    it.animate().rotationBy(180f).setDuration(100).start()
                    msgText.visibility = View.GONE
                    item.visible = false
                } else {
                    it.animate().rotationBy(-180f).setDuration(100).start()
                    msgText.visibility = View.VISIBLE
                    item.visible = true
                }
            }
        }
    }
}