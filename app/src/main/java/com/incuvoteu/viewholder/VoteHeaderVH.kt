package com.incuvoteu.viewholder

import androidx.databinding.ViewDataBinding
import com.incuvoteu.databinding.VoteHeaderListItemBinding
import com.incuvoteu.model.VoteHeaderItem

class VoteHeaderVH : BaseViewHolder {
   lateinit var binding : VoteHeaderListItemBinding

    constructor(_binding : ViewDataBinding) : super(_binding.root){
        if(_binding is VoteHeaderListItemBinding) {
            this.binding = _binding
        }
    }

    override fun bind(item: Any?) {
        item?.let {
            if(item is VoteHeaderItem){
                binding.header = item
            }
        }
    }
}