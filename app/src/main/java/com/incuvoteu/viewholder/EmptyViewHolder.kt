package com.leo.kakaocompleterecyclerview.viewholder

import android.view.View
import com.incuvoteu.viewholder.BaseViewHolder

class EmptyViewHolder(itemView : View) : BaseViewHolder(itemView) {
    override fun bind(_item: Any?) {

    }
}