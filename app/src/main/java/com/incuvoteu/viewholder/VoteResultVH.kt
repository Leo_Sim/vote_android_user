package com.incuvoteu.viewholder

import androidx.databinding.ViewDataBinding
import com.incuvoteu.databinding.VoteResultListItemBinding
import com.incuvoteu.model.ReceivedVoteOptionItem

class VoteResultVH : BaseViewHolder {
    lateinit var binding : VoteResultListItemBinding
    constructor(_binding : ViewDataBinding) : super(_binding.root) {
        if(_binding is VoteResultListItemBinding) {
            binding = _binding
        }
    }

    override fun bind(item: Any?) {
        if(item is ReceivedVoteOptionItem){
            binding.item = item
        }
    }
}