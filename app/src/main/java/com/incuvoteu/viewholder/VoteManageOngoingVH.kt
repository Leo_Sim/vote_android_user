package com.incuvoteu.viewholder

import android.content.Intent
import android.view.View
import com.incuvoteu.ui.main.VoteManageEditActivity

class VoteManageOngoingVH : BaseViewHolder {
    constructor(itemView: View) : super(itemView) {
        itemView.setOnClickListener {
            val intent = Intent(itemView.context, VoteManageEditActivity::class.java)
            itemView.context.startActivity(intent)
        }
    }

    override fun bind(item: Any?) {

    }
}