package com.incuvoteu.viewholder

import android.view.View
import androidx.databinding.ViewDataBinding
import com.incuvoteu.databinding.MainHomeVoteListItemBinding
import com.incuvoteu.model.ReceivedUserVoteItem

open class VoteViewHolder : BaseViewHolder {
    lateinit var binding : MainHomeVoteListItemBinding

    constructor(itemView : View) : super(itemView)
    constructor(_binding: ViewDataBinding) : super(_binding.root) {
        if(_binding is MainHomeVoteListItemBinding){
            this.binding = _binding
        }
    }

    override fun bind(item: Any?) {
        if (item is ReceivedUserVoteItem) {
            item.let {
                binding.item = item
            }
        }
    }
}