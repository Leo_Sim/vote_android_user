package com.incuvoteu.viewholder

import android.content.Intent
import androidx.lifecycle.ViewModel
import com.incuvoteu.databinding.VoteOptionListItemBinding

import com.incuvoteu.manager.ImageManager
import com.incuvoteu.model.VoteOptionItem
import com.incuvoteu.ui.main.MainActivity
import com.incuvoteu.ui.main.VoteManageEditActivity
import com.incuvoteu.ui.main.viewmodel.votemanage.VoteManageFragmentStep2ViewModel


class VoteManageItemVH : BaseViewHolder {
    private val binding: VoteOptionListItemBinding
    private lateinit var parentViewmodel: VoteManageFragmentStep2ViewModel

    constructor(
        _parentViewmodel: ViewModel,
        _binding: VoteOptionListItemBinding
    ) : super(_binding.root) {
        binding = _binding
        if (_parentViewmodel is VoteManageFragmentStep2ViewModel) {
            parentViewmodel = _parentViewmodel
        }

    }

    override fun bind(item: Any?) {
        if (item is VoteOptionItem) {
            binding.deleteBtn.setOnClickListener { parentViewmodel.deleteOption(item) }
            binding.viewmodel!!.init(item)
            binding.fileBtn.setOnClickListener {
                val viewmodel = binding.viewmodel
                ImageManager.lastPickedLiveData = viewmodel!!.imgUri
                val intent = Intent(Intent.ACTION_PICK)
                intent.setDataAndType(
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    "image/*"
                )
                val mContext = itemView.context
                if(mContext is MainActivity){
                    mContext.startActivityForResult(intent, MainActivity.OPEN_IMAGE_GALLERY)
                }
                if(mContext is VoteManageEditActivity){
                    mContext.startActivityForResult(intent, MainActivity.OPEN_IMAGE_GALLERY)
                }
            }
        }
    }
}