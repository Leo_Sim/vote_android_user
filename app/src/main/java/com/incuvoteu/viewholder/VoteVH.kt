package com.incuvoteu.viewholder

import android.util.Log
import androidx.databinding.ViewDataBinding
import com.incuvoteu.api.client.VoteApiClient
import com.incuvoteu.databinding.VoteListItemBinding
import com.incuvoteu.databinding.VoteResultListItemBinding
import com.incuvoteu.model.ReceivedVoteOptionItem
import com.incuvoteu.util.ToastUtil

class VoteVH : BaseViewHolder {
    lateinit var binding: VoteListItemBinding
    private var voteId: Int = -1

    constructor(_binding: ViewDataBinding, voteId: Int) : super(_binding.root) {
        this.voteId = voteId
        if (_binding is VoteListItemBinding) {
            binding = _binding
        }
    }

    override fun bind(item: Any?) {
        if (item is ReceivedVoteOptionItem) {
            Log.d("devsim","progress :" + item.percentage)
            binding.item = item
            binding.voteBtn.setOnClickListener {
                VoteApiClient.voting(voteId, item.id, 1).subscribe({
                    ToastUtil.make("투표 완료!")
                }, { err -> ToastUtil.make(err.message) })
            }
        }
    }
}