package com.incuvoteu.viewholder

import android.content.Intent
import android.widget.Toast
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.incuvoteu.model.NavMenuItem
import com.incuvoteu.databinding.MainHomeVoteListItemBinding
import com.incuvoteu.databinding.MypageMenuItemBinding

class MenuViewHolder : BaseViewHolder {
    lateinit var binding: MypageMenuItemBinding

    constructor(_binding: ViewDataBinding) : super(_binding.root) {
        if (_binding is MypageMenuItemBinding) {
            this.binding = _binding
        }
    }

    override fun bind(item: Any?) {
        if(item is NavMenuItem){
            Glide.with(itemView.context)
                .load(item.drawableRes)
                .into(binding.menuItemIcon)
            binding.menuItemTitle.text = item.title

            itemView.setOnClickListener {
                Toast.makeText(itemView.context,"${item.title} 클릭!",Toast.LENGTH_SHORT).show()
                it.context.startActivity(Intent(it.context,item.targetActivity))
            }
        }
    }
}