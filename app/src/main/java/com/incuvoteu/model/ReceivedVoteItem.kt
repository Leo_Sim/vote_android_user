package com.incuvoteu.model

import com.incuvoteu.api.FetchState

/**
 * 리스트 조회용
 */
data class ReceivedWrapper<T> (
    val content : List<T>,
    val last : Boolean,
    var fetchState: FetchState
)