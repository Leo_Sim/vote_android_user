package com.incuvoteu.model

import com.google.gson.annotations.SerializedName

/**
 * 투표 관리, 생성에 사용되는 아이템
 */
class VoteOptionItem {
    @SerializedName("id") var id : Int? = -1
    @SerializedName("title") var title : String = "Default" //선택지
    @SerializedName("description") var desc: String = "" //설명
    var imgUri: String? = "" //첨부파일 주소
    @SerializedName("fileId") var fileId : Int? = -1
    fun createDefaultItem() : VoteOptionItem = VoteOptionItem()

    override fun toString(): String {
        return "id : " + id + "| + title : " + title + " | desc : " + desc + " | imgUri : " + imgUri + " | fileid : " + fileId
    }
}