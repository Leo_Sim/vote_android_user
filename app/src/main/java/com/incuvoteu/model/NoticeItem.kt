package com.incuvoteu.model

data class NoticeItem(
    val title : String,
    val msg : String,
    var visible : Boolean = false
)