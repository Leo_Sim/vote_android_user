package com.incuvoteu.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReceivedVoteOptionItem(
    val id : Int,
    val description : String,
    val imageUrl : String?,
    val title : String,
    val fileId : Int?,
    val percentage : Float,
    val count : Int
) : Parcelable