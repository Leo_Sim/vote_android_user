package com.incuvoteu.model

data class FileImage(
    val id : Int? = 0,
    val fileName : String? = "-"
)