package com.incuvoteu.model

data class User (
    val email : String,
    val password : String,
    val rePassword : String,
    val mobile : String
)