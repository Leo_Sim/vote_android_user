package com.incuvoteu.model

import android.content.Context
import androidx.annotation.DrawableRes

data class NavMenuItem(
    val title: String,
    @DrawableRes val drawableRes: Int,
    val targetActivity: Class<out Context>
)