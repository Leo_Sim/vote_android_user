package com.incuvoteu.model

/** UI용 **/
data class VoteItem(
    var id : Int? = -1,
    var title : String? = "",
    var desc : String? = "",
    var startDate : Long? = -1,
    var endDate : Long? = -1,
    var isPaid : Int? = 0,
    var isMultipleChoice : Int? = 0,
    var isMultiple : Int? = 0,
    var isSecret : Int? = 0,
    var isOpen : Int? = 0
){
    companion object {
        val ENUM_IS_PAID = HashMap<String,Int>().apply {
            put("PAID_N",0)
            put("PAID_Y",1)
        }
        val ENUM_IS_MULTIPLE = HashMap<String,Int>().apply {
            put("MULTIPLE_N",0)
            put("MULTIPLE_Y",1)
        }
        val ENUM_IS_MULTIPLE_CHOICE = HashMap<String,Int>().apply {
            put("MULTIPLE_CHOICE_N",0)
            put("MULTIPLE_CHOICE_Y",1)
        }
        //투표방법
        val ENUM_IS_OPEN = HashMap<String,Int>().apply {
            put("OPEN_Y",0)
            put("OPEN_N",1)
        }
        val ENUM_IS_SECRET = HashMap<String,Int>().apply {
            put("SECRET_Y",0)
            put("SECRET_N",1)
        }
        private val ENUM_VOTE_TYPE = HashMap<String,Int>().apply {
            put("REAL_TIME_VOTE",0)
            put("PRE_VOTE",1)
            put("EVENT_VOTE",2)
        }

        fun convertSwitchIdxToString(type : Map<String,Int>, idx : Int?) : String{
            for(i in type){
                if(i.value == idx){
                    return i.key
                }
            }
            return "none"
        }

        fun convertStringToSwitchIdx(type : Map<String,Int>, string : String) : Int{
            val k = type.get(string)
            if(k != null) return k
            return 0
        }
    }
}