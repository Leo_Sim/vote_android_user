package com.incuvoteu.model

import lombok.ToString

@ToString
data class UserVoteItem (
    val id : Int? = 0,
    val broadcasterId : Int? = -1,
    val title : String? = "default",
    val description : String? = "default desc",
    val startDate : Long? = 1586759760,
    val endDate : Long? = 1586759767,
    val isPaid : String?,
    val isMultiple : String?,
    val isMultipleChoice: String?,
    val isSecret : String?,
    val isOpen : String?,
    val programId : Int?,
    var voteOptions : List<VoteOptionItem>?,
    val voteType : String?
)