package com.incuvoteu.model

data class VoteHeaderItem(
    val total : Int = 0,
    val ongoing : Int = 0,
    val complete : Int = 0
)