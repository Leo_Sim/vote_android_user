package com.incuvoteu.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReceivedUserVoteItem(
    val id : Int,
    val title : String,
    val description : String,
    val startDate : Long,
    val endDate : Long,
    var startDateStr : String,
    var endDateStr : String,
    val isPaid : String,
    val isMultiple : String,
    val isMultipleChoice: String,
    val isOpen : String,
    val isSecret : String,
    val voteOptions : List<ReceivedVoteOptionItem>?,
    val count : Int = 0
) : Parcelable