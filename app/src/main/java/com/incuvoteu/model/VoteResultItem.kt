package com.incuvoteu.model

data class VoteResultItem(
    val title : String?,
    val group : String?,
    val imgUri : String?,
    val ratio : Float?
)