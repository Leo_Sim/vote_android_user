package com.incuvoteu.model

data class VoteListItem(
    val theme : String?,
    val title : String?,
    val startDate : String?,
    val endDate : String?,
    val viewCount : Int,
    val replyCount : Int,
    val imgUri : String?
)