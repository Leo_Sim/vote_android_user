package com.incuvoteu.ui.notice

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.incuvoteu.R
import com.incuvoteu.model.NoticeItem
import kotlinx.android.synthetic.main.top_toolbar.*

class NoticeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notice)
        initToolbar()
        
        val items = arrayListOf(
            NoticeItem(
                "2019-07-23 더쇼 사전투표 시작",
                resources.getString(R.string.test_notice_msg)
            ),
            NoticeItem(
                "2019-07-08 일시 장애 공지",
                "죄송합니다"
            )

        )

//        val noticeAdapter = SingleBindingRecyclerAdapter(R.layout.notice_list_item, NoticeViewHolder::class)
//        noticeAdapter.additem(items)
//        noticeRecyclerView.layoutManager = LinearLayoutManager(this)
//        noticeRecyclerView.adapter = noticeAdapter
    }

    private fun initToolbar(){
        toolbarTtitle.text = "공지사항"
        toolbarBackBtn.setOnClickListener { finish() }
    }
}
