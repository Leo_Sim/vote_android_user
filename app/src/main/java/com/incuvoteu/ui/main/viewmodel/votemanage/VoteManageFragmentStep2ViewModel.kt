package com.incuvoteu.ui.main.viewmodel.votemanage

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.incuvoteu.model.ReceivedVoteOptionItem
import com.incuvoteu.model.VoteOptionItem

class VoteManageFragmentStep2ViewModel : ViewModel(){
    val voteItems : MutableLiveData<MutableList<VoteOptionItem>> = MutableLiveData()

    init {
        voteItems.value = mutableListOf(VoteOptionItem().createDefaultItem())
    }

    fun addVoteItem(){
        val items = voteItems.value
        items?.add(VoteOptionItem().createDefaultItem())
        voteItems.value = items
    }

    fun check(){
        Log.d("devsim","ccc")
        val item = voteItems.value
        val size = item?.size
        for(i in item!!) {
            Log.d("devsim", "str : " + i.title)
            Log.d("devsim", "uri : " + i.imgUri)
            Log.d("devsim","desc : " + i.desc)
        }
    }

    fun deleteOption(item : VoteOptionItem){
        val items = voteItems.value
        items!!.remove(item)
        voteItems.value = items
    }

    fun load(items : List<ReceivedVoteOptionItem>){
        val datas = mutableListOf<VoteOptionItem>()
        for(i in items){
            val newVoteOption = VoteOptionItem()
            Log.d("devsim","back : id " + i.id)
            with(newVoteOption){
                id = i.id
                title = i.title
                desc = i.description
                imgUri = i.imageUrl
                fileId = i.fileId
            }

            datas.add(newVoteOption)
        }
        voteItems.value = datas
    }

    fun clear(){
        voteItems.value = mutableListOf(VoteOptionItem().createDefaultItem())
    }
}