package com.incuvoteu.ui.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.incuvoteu.api.FetchState
import com.incuvoteu.api.client.VoteApiClient
import com.incuvoteu.model.ReceivedUserVoteItem

class OnGoingViewModel : ViewModel() {
    val listItem: MutableLiveData<List<ReceivedUserVoteItem>> = MutableLiveData()
    var fetchState: FetchState = FetchState()

    fun loadMore(page: Int, size: Int) {
        fetchState.isOnLoading = true
        VoteApiClient.fetchOpenAll(page,size)
            .subscribe({ env ->
                fetchState = env.fetchState
                listItem.value = env.content
            }, { err -> fetchState.isOnError = true
            })
    }

    override fun onCleared() {
        super.onCleared()
    }
}