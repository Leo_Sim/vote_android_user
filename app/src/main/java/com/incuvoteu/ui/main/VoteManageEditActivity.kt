package com.incuvoteu.ui.main

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.incuvoteu.R
import com.incuvoteu.api.client.VoteApiClient
import com.incuvoteu.fragment.main.MainVoteCreateFragment
import com.incuvoteu.manager.ImageManager
import com.incuvoteu.util.ToastUtil

class VoteManageEditActivity : AppCompatActivity() {
    var voteId : Int = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vote_manage_edit)
        initIntent()

        VoteApiClient.fetch(voteId).subscribe({
            val bundle = Bundle()
            val f = MainVoteCreateFragment()
            bundle.putParcelable("voteItem",it)
            f.arguments = bundle
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment, f)
                .commit()
        },{err -> ToastUtil.make(err.message)})


    }

    private fun initIntent(){
        voteId = intent.getIntExtra("voteId",-1)
        Log.d("devsim","voteId : " + voteId)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == MainActivity.OPEN_IMAGE_GALLERY){
            if(resultCode == RESULT_OK){
                try{
                    val uri = data?.data
                    Log.d("devsim","uri :  " + uri?.path)
                    ImageManager.lastPickedImg = uri.toString()
                }catch(e : Exception){
                    e.printStackTrace()
                }
            } else if(resultCode == Activity.RESULT_CANCELED){
                Toast.makeText(this, "사진 선택 취소", Toast.LENGTH_LONG).show()
            }
        }
    }
}

