package com.incuvoteu.ui.main

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.incuvoteu.R
import com.incuvoteu.adapter.base.SingleBindingRecyclerAdapter
import com.incuvoteu.adapter.base.VoteBindingRecyclerAdapter
import com.incuvoteu.api.client.VoteApiClient
import com.incuvoteu.util.ToastUtil
import com.incuvoteu.viewholder.VoteResultVH
import com.incuvoteu.viewholder.VoteVH
import kotlinx.android.synthetic.main.activity_vote_result.*
import kotlinx.android.synthetic.main.top_toolbar.*

class VoteActivity : AppCompatActivity() {
    private lateinit var adapter : SingleBindingRecyclerAdapter
    private var id : Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vote_result)
        initTitle()
        initIntent()
        initList()

        VoteApiClient.fetch(id).subscribe({
            Log.d("devsim","zz id : " + it.id)
            titleTxt.text = it.title
            descTxt.text = it.description
            val options = it.voteOptions
            for(i in options!!){
                Log.d("devsim","zz option id : " + i.id)
            }
            adapter.addItems(options!!)
        },{err -> ToastUtil.make(err.message)})
    }

    private fun initTitle(){
        toolbarTtitle.text = "투표하기"
        toolbarBackBtn.setOnClickListener { finish() }
   }

    private fun initIntent(){
        id = intent.getIntExtra("voteId",-1)
    }

    private fun initList(){
        adapter = VoteBindingRecyclerAdapter(id,R.layout.vote_list_item, VoteVH::class)
        recyclerView.adapter = adapter
        val lm = LinearLayoutManager(this)
        recyclerView.layoutManager = lm
    }
}
