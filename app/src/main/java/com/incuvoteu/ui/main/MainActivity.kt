package com.incuvoteu.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import com.incuvoteu.R
import com.incuvoteu.adapter.base.BasePagerAdapter
import com.incuvoteu.fragment.main.*
import com.incuvoteu.manager.ImageManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    companion object {
        val OPEN_IMAGE_GALLERY = 0
    }
    lateinit var pagerAdapter : BasePagerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        val file = File(FileUtil.getPath(this,Uri.parse("content://media/external/images/media/2078")))
//        val fbody = file.asRequestBody("image/*".toMediaTypeOrNull())
//        FileApiClient.upload(fbody,"VOTE_OPTION").subscribe({
//            t -> Log.d("devsim","file upload complete")
//        },{err -> ToastUtil.make(err.message)})

        val pages = arrayListOf(
            Pair("홈", MainHomeFragment::class),
            Pair("투표하기", MainVoteFragment::class),
            Pair("스토어", MainStoreFragment::class),
            Pair("마이페이지", MainMyPageFragment::class)
            )

        pagerAdapter = BasePagerAdapter(supportFragmentManager,pages)
        pager.adapter = pagerAdapter
        pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout.removeAllTabs()
        for(i in pages) {
            val pageName = i.first
            tabLayout.addTab(tabLayout.newTab().setText(pageName))
        }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                pager.setCurrentItem(tab!!.position, false)
            }
        })
    }

    fun goMain(){
        val frs = supportFragmentManager.fragments
        pager.setCurrentItem(0,false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == OPEN_IMAGE_GALLERY){
            if(resultCode == RESULT_OK){
                try{
                    val uri = data?.data
                    Log.d("devsim","uri :  " + uri?.path)
                    ImageManager.lastPickedImg = uri.toString()
                }catch(e : Exception){
                    e.printStackTrace()
                }
            } else if(resultCode == Activity.RESULT_CANCELED){
                Toast.makeText(this, "사진 선택 취소", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onBackPressed() {
        finishAffinity()
    }
}
