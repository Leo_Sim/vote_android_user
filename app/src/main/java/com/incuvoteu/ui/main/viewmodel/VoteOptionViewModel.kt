package com.incuvoteu.ui.main.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.incuvoteu.model.VoteOptionItem

class VoteOptionViewModel : ViewModel() {

    lateinit var item: VoteOptionItem

    var title: MutableLiveData<String> = MutableLiveData() //선택지 제목
    var desc: MutableLiveData<String> = MutableLiveData() //선택지 설명
    var imgUri: MutableLiveData<String> = MutableLiveData() //선택지 사진 URL

    /**
     * 원본 데이터에 수정사항 적용
     */
    var saveMediator = MediatorLiveData<Unit>().apply {
        addSource(title) { item.title = it }
        addSource(desc) { item.desc = it }
        addSource(imgUri) { item.imgUri = it }
    }

    fun init(_item: VoteOptionItem) {
        item = _item
        title.value = item.title
        desc.value = item.desc
        imgUri.value = item.imgUri

    }
}