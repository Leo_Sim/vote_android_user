package com.incuvoteu.ui.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.incuvoteu.api.client.UserApiClient
import com.incuvoteu.util.ToastUtil

class MainMyPageFragmentViewModel : ViewModel(){
    val username : MutableLiveData<String> = MutableLiveData()

    fun loadUserInfo(){
        UserApiClient.get().subscribe({
                user -> username.value = user.nickName
        },{err -> ToastUtil.make(err.message)})

    }
}