package com.incuvoteu.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.incuvoteu.R
import com.incuvoteu.api.client.UserApiClient
import com.incuvoteu.util.ToastUtil
import kotlinx.android.synthetic.main.activity_my_account_manage.*
import kotlinx.android.synthetic.main.top_toolbar.*

class MyAccountManageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account_manage)
        initToolbar()
        submitBtn.setOnClickListener { usernameUpdate() }
        loadUserInfo()
    }

    private fun initToolbar(){
        toolbarTtitle.text = "내 정보 관리"
        toolbarBackBtn.setOnClickListener { finish() }
    }

    private fun loadUserInfo() {
        UserApiClient.get().subscribe({ user ->
            usernameEdit.setText(user.nickName)
        }, { err -> ToastUtil.make(err.message) })
    }

    private fun usernameUpdate() {
        UserApiClient.update(usernameEdit.text.toString()).subscribe({
            ToastUtil.make("닉네임 변경 완료!")
            finish()
        }, { err -> ToastUtil.make(err.message) })
    }
}
