package com.incuvoteu.ui.main.viewmodel.votemanage

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.incuvoteu.model.ReceivedUserVoteItem
import com.incuvoteu.model.VoteItem
import java.util.*

class VoteManageFragmentStep1ViewModel : ViewModel() {
    var id: Int? = -1
    val title: MutableLiveData<String> = MutableLiveData()
    val desc: MutableLiveData<String> = MutableLiveData()

    val startDate: MutableLiveData<Calendar> = MutableLiveData()
    val endDate: MutableLiveData<Calendar> = MutableLiveData()

    val startDateStr = Transformations.map(startDate) {
        calendarToString(it)
    }
    val endDateStr = Transformations.map(endDate) {
        calendarToString(it)
    }

    val isPaid: MutableLiveData<Int> = MutableLiveData<Int>()
    val isMultipleChoice: MutableLiveData<Int> = MutableLiveData<Int>()
    val isMultiple: MutableLiveData<Int> = MutableLiveData<Int>()
    val isSecret: MutableLiveData<Int> = MutableLiveData<Int>()
    val isOpen: MutableLiveData<Int> = MutableLiveData<Int>()

    init {
//        startDate.value = Calendar.getInstance()
//        endDate.value = Calendar.getInstance()
        isPaid.value = -1
        isMultipleChoice.value = -1
        isMultiple.value = -1
        isSecret.value = -1
        isOpen.value = -1
    }

    /**
     * M/D/Y
     */
    private fun calendarToString(calendar: Calendar): String {
        val m = calendar.get(Calendar.MONTH) + 1
        val d = calendar.get(Calendar.DATE)
        val y = calendar.get(Calendar.YEAR)
        val sb = StringBuilder()
        with(sb) {
            append(m).append("/").append(d).append("/").append(y)
        }
        return sb.toString()
    }

    fun getStartDate(type: Int): Int {
        var cal = startDate.value
        checkNotNull(cal) {
            return Calendar.getInstance().get(type) //today
        }
        return cal.get(type)
    }

    fun getEndDate(type: Int): Int {
        var cal = endDate.value
        checkNotNull(cal) {
            return Calendar.getInstance().get(type) //today
        }
        return cal.get(type)
    }

    fun load(item: ReceivedUserVoteItem?) {
        if (item != null) {
            id = item.id
            title.value = item.title
            desc.value = item.description
            val sCal = Calendar.getInstance()
            sCal.timeInMillis = item.startDate * 1000
            startDate.value = sCal
            val eCal = Calendar.getInstance()
            eCal.timeInMillis = item.endDate * 1000
            endDate.value = eCal
            isPaid.value = VoteItem.convertStringToSwitchIdx(VoteItem.ENUM_IS_PAID, item.isPaid)
            isMultipleChoice.value = VoteItem.convertStringToSwitchIdx(
                VoteItem.ENUM_IS_MULTIPLE_CHOICE,
                item.isMultipleChoice
            )
            isMultiple.value =
                VoteItem.convertStringToSwitchIdx(VoteItem.ENUM_IS_MULTIPLE, item.isMultiple)
            isSecret.value =
                VoteItem.convertStringToSwitchIdx(VoteItem.ENUM_IS_SECRET, item.isSecret)
            Log.d("devsim", "isO : " + item.isOpen)
            isOpen.value = VoteItem.convertStringToSwitchIdx(VoteItem.ENUM_IS_OPEN, item.isOpen)
        }
    }

    fun clear(){
        id = -1
        title.value = ""
        desc.value = ""
        startDate.value = Calendar.getInstance()
        endDate.value = Calendar.getInstance()
        isPaid.value = 0
        isMultipleChoice.value = 0
        isMultiple.value = 0
        isSecret.value = 0
        isOpen.value = 0
    }
}