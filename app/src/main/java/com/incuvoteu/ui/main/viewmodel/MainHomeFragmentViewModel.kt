package com.incuvoteu.ui.main.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.incuvoteu.api.FetchState
import com.incuvoteu.api.client.VoteApiClient
import com.incuvoteu.model.ReceivedUserVoteItem

class MainHomeFragmentViewModel : ViewModel(){
    val listItem: MutableLiveData<List<ReceivedUserVoteItem>> = MutableLiveData()
    var fetchState: FetchState = FetchState()

    fun loadMore(page: Int, size: Int) {
        Log.d("devsim", "more11111 " + page)
        fetchState.isOnLoading = true
        VoteApiClient.fetchOpenAll(page,size)
            .subscribe({ env ->
                fetchState = env.fetchState
                listItem.value = env.content
                Log.d("devsim","pp size : " + env.content.size)
            }, { err -> fetchState.isOnError = true
            })
    }

    override fun onCleared() {
        super.onCleared()
        Log.d("devsim","onlcear")
    }
}