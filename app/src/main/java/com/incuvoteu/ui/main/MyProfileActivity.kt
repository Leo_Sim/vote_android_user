package com.incuvoteu.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.incuvoteu.R

class MyProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile)
    }
}
