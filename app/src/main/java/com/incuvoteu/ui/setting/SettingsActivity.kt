package com.incuvoteu.ui.setting

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.incuvoteu.R
import com.incuvoteu.manager.AuthManager
import com.incuvoteu.ui.login.LoginActivity
import com.kakao.network.ErrorResult
import com.kakao.usermgmt.UserManagement
import com.kakao.usermgmt.callback.LogoutResponseCallback
import com.kakao.usermgmt.callback.UnLinkResponseCallback
import kotlinx.android.synthetic.main.settings_activity.*
import kotlinx.android.synthetic.main.top_toolbar.*
import kotlin.reflect.KClass

class SettingsActivity : AppCompatActivity() {

    private lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        this.context = this
        initToolbar()

        service.setOnClickListener { callActivity(context, SettingServiceActivity::class) }
        language.setOnClickListener { callActivity(context, SettingLanguageActivity::class) }
        secede.setOnClickListener { callActivity(context, SettingSecedeActivity::class) }
        logout.setOnClickListener { logout() }
        notice.setOnClickListener { callActivity(context, SettingAlarmActivity::class) }

    }

    private fun initToolbar() {
        toolbarTtitle.text = "설정"
        toolbarBackBtn.setOnClickListener { finish() }
    }

    private fun logout() {
        if (AuthManager.type == AuthManager.SOCIAL_TYPE.KAKAO.value) {
            val th = object : Thread() {
                override fun run() {
                    UserManagement.getInstance()
                        .requestLogout(object : LogoutResponseCallback() {
                            override fun onCompleteLogout() {
                                AuthManager.logout()
                                runOnUiThread {
                                    Toast.makeText(context, "로그아웃 성공!", Toast.LENGTH_LONG).show()
                                    val i = Intent(context, LoginActivity::class.java)
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                    context.startActivity(i)
                                }
                            }
                        })
                }
//            val th = object : Thread() {
//                override fun run() {
//                    UserManagement.getInstance()
//                        .requestUnlink(object : UnLinkResponseCallback() {
//                            override fun onSuccess(result: Long?) {
//                                AuthManager.logout()
//                                runOnUiThread {
//                                    Toast.makeText(context, "로그아웃 성공!", Toast.LENGTH_LONG).show()
//                                    val i = Intent(context, LoginActivity::class.java)
//                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
//                                    context.startActivity(i)
//                                }
//                            }
//
//                            override fun onFailure(errorResult: ErrorResult?) {
//                                super.onFailure(errorResult)
//                            }
//
//                            override fun onSessionClosed(errorResult: ErrorResult?) {
//                            }
//                        })
//                }
            }
            th.start()
        }
    }

    private fun callActivity(context: Context, activity: KClass<out AppCompatActivity>) {
        val i = Intent(context, activity.java)
        startActivity(i)
    }

}