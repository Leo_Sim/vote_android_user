package com.incuvoteu.ui.setting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.incuvoteu.R
import kotlinx.android.synthetic.main.top_toolbar.*

class SettingLanguageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_language)
        initToolbar()
    }

    private fun initToolbar(){
        toolbarTtitle.text = "언어설정"
        toolbarBackBtn.setOnClickListener { finish() }
    }
}
