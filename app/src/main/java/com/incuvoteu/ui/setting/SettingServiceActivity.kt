package com.incuvoteu.ui.setting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import com.incuvoteu.R
import com.incuvoteu.adapter.base.BasePagerAdapter
import com.incuvoteu.fragment.main.service.SettingService1
import com.incuvoteu.fragment.main.service.SettingService2
import com.incuvoteu.fragment.main.service.SettingService3
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_sign_up.pager
import kotlinx.android.synthetic.main.top_toolbar.*

class SettingServiceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_service)
        initToolbar()
        initPager()
    }

    private fun initToolbar(){
        toolbarTtitle.text = "서비스 정보"
        toolbarBackBtn.setOnClickListener { finish() }
    }

    private fun initPager() {
        val pages = arrayListOf(
            Pair("서비스이용약관", SettingService1::class),
            Pair("개인정보 취급방침", SettingService2::class),
            Pair("오픈소스 라이선스", SettingService3::class)
        )

        val pagerAdapter = BasePagerAdapter(supportFragmentManager, pages)
        pager.adapter = pagerAdapter

        pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.removeAllTabs()
        tabLayout.addTab(tabLayout.newTab().setText("서비스이용약관"))
        tabLayout.addTab(tabLayout.newTab().setText("개인정보 취급방침"))
        tabLayout.addTab(tabLayout.newTab().setText("오픈소스 라이선스"))


        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                pager.setCurrentItem(tab!!.position, false)
            }
        })
    }
}
