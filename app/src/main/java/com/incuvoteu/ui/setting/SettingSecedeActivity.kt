package com.incuvoteu.ui.setting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.incuvoteu.R
import kotlinx.android.synthetic.main.top_toolbar.*

class SettingSecedeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_secede)
        initToolbar()
    }

    private fun initToolbar(){
        toolbarTtitle.text = "서비스 계정 탈퇴"
        toolbarBackBtn.setOnClickListener { finish() }
    }
}
