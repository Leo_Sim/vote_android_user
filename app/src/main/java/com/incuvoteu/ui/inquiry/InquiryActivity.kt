package com.incuvoteu.ui.inquiry

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import com.incuvoteu.R
import com.incuvoteu.adapter.base.BasePagerAdapter
import com.incuvoteu.fragment.inquiry.FragmentInquiry1
import com.incuvoteu.fragment.inquiry.FragmentInquiry2
import com.incuvoteu.fragment.inquiry.FragmentInquiry3
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_sign_up.pager
import kotlinx.android.synthetic.main.top_toolbar.*

class InquiryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inquiry)
        initToolbar()
        initPager()
    }

    private fun initToolbar(){
        toolbarTtitle.text = "문의하기"
        toolbarBackBtn.setOnClickListener { finish() }
    }

    private fun initPager() {
        val pages = arrayListOf(
            Pair("자주묻는질문", FragmentInquiry1::class),
            Pair("문의하기", FragmentInquiry2::class),
            Pair("나의문의내역", FragmentInquiry3::class)
        )

        val pagerAdapter = BasePagerAdapter(supportFragmentManager, pages)
        pager.adapter = pagerAdapter

        pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.removeAllTabs()
        tabLayout.addTab(tabLayout.newTab().setText("자주묻는질문"))
        tabLayout.addTab(tabLayout.newTab().setText("문의하기"))
        tabLayout.addTab(tabLayout.newTab().setText("나의문의내역"))


        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                pager.setCurrentItem(tab!!.position, false)
            }
        })
    }
}
