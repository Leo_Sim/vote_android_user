package com.incuvoteu.ui.customViews

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager


class BaseViewPager : ViewPager{
    private var isPagingEnabled = false

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    fun setPagingEnabled(flag : Boolean){
        isPagingEnabled = flag
    }

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        return  this.isPagingEnabled && super.onTouchEvent(ev)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return this.isPagingEnabled && super.onInterceptTouchEvent(ev)
    }
}