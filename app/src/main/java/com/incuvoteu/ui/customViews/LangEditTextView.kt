package com.incuvoteu.ui.customViews

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import com.incuvoteu.manager.LangBroadcastManager

class LangEditTextView : AppCompatEditText {
    private lateinit var langChangeBroadcast: LangBroadcastManager.OnLangChangeBroadcast

    constructor(context: Context) : super(context)
    constructor(
        context: Context,
        attrs: AttributeSet
    ) : super(context, attrs) {
        langChangeBroadcast = LangBroadcastManager.buildLangBroadcast(this, attrs)
        LangBroadcastManager.addLangChangeBroadcast(langChangeBroadcast)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        LangBroadcastManager.removeLangChangeBroadcast(langChangeBroadcast)
    }
}