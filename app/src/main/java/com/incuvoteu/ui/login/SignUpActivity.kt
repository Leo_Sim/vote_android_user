package com.incuvoteu.ui.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.incuvoteu.R
import com.incuvoteu.adapter.base.BasePagerAdapter
import com.incuvoteu.api.client.SignApiClient
import com.incuvoteu.fragment.login.SignUpStep1Fragment
import com.incuvoteu.fragment.login.SignUpStep2Fragment
import com.incuvoteu.fragment.login.SignUpStep3Fragment
import com.incuvoteu.util.ToastUtil
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    val MAX_STEP_PAGE = 3
    var step = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        initPager()

//        SignApiClient.signup(User("22df@naver.com","123","123","123")).subscribe({
//            Log.d("devsim","signup! success")
//            ToastUtil.make("회원가입 완료!")
//            finish()
//        },{
//                err -> ToastUtil.make(err.message)
//            Log.d("devsim","signup! fail")
//        })
    }

    fun nextStep(){
        step++
        if(step > MAX_STEP_PAGE){
            singup()
        }else {
            pager.setCurrentItem(step-1,false)
        }
    }

    private fun singup(){
        val key = "android:switcher:"+pager.id+":1"
        val fr = supportFragmentManager.findFragmentByTag("android:switcher:"+pager.id+":0")
        val signupUser = (fr as SignUpStep1Fragment).getSignupData()

        Log.d("devsim","signup!")
        SignApiClient.signup(signupUser).subscribe({
            Log.d("devsim","signup! success")
            ToastUtil.make("회원가입 완료!")
            finish()
        },{
            err -> ToastUtil.make(err.message)
            Log.d("devsim","signup! fail")
        })
    }

    private fun initPager(){
        val pages = arrayListOf(
            Pair("Step1", SignUpStep1Fragment::class),
            Pair("Step2", SignUpStep2Fragment::class),
            Pair("Step3", SignUpStep3Fragment::class)
            )
        val pagerAdapter = BasePagerAdapter(supportFragmentManager,pages)
        pager.adapter = pagerAdapter
    }
}
