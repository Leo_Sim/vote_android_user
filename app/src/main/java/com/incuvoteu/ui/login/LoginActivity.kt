package com.incuvoteu.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.incuvoteu.R
import com.incuvoteu.api.client.SignApiClient
import com.incuvoteu.manager.AuthManager
import com.incuvoteu.ui.main.MainActivity
import com.incuvoteu.util.ToastUtil
import com.kakao.auth.ISessionCallback
import com.kakao.auth.Session
import com.kakao.util.exception.KakaoException
import com.kakao.util.helper.log.Logger
import timber.log.Timber

/**
 *  로그인
 */
class LoginActivity : AppCompatActivity() {
    private lateinit var kakaoSessionCallback: SessionCallback
    private lateinit var mContext: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mContext = this
        autoSignin()
    }

    private fun autoSignin() {
        initKakaoSession()
    }

    /**
     * 카카오로그인 세션 초기화
     */
    private fun initKakaoSession() {
        kakaoSessionCallback = SessionCallback()
        Session.getCurrentSession().addCallback(kakaoSessionCallback)
        Session.getCurrentSession().checkAndImplicitOpen() // Session 상태 체크
    }

    /**
     * 로그인 액티비티 이후에 sdk에서 필요로 하는 activity를 띄우도록
     * onActivityResult의 결과를 sdk handleActivityResult에 위임.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    inner class SessionCallback : ISessionCallback {
        override fun onSessionOpenFailed(exception: KakaoException?) {
            Timber.d("onSessionOpenFailed")
            if (exception != null) {
                Logger.e(exception)
            }
        }

        /**
         * 인증 성공시 메인으로 이동
         */
        override fun onSessionOpened() {
            Timber.d("onSessionOpened")
            Timber.d("Access token : %s", Session.getCurrentSession().tokenInfo.accessToken)
            Timber.d("Refresh token : %s", Session.getCurrentSession().tokenInfo.refreshToken)
            SignApiClient.signin("kakao",Session.getCurrentSession().tokenInfo.accessToken).subscribe({
                token -> Log.d("devsim","kakao success : " + token)
                AuthManager.type = AuthManager.SOCIAL_TYPE.KAKAO.value
                AuthManager.kakaoToken = token
                startActivity(Intent(mContext, MainActivity::class.java))
                finish()
            },{err -> ToastUtil.make(err.message)})
        }
    }
}
