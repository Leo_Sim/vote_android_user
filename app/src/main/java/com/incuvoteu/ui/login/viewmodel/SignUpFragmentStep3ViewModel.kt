package com.incuvoteu.ui.login.viewmodel

import androidx.lifecycle.*
import timber.log.Timber

class SignUpFragmentStep3ViewModel : ViewModel() {

    var termsCheck : MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false}
    var validTermsCheck : LiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }

    var checkerManager : MediatorLiveData<Boolean> = MediatorLiveData()

    var btnEnable : LiveData<Boolean>

    init{

        validTermsCheck = Transformations.map(termsCheck) {chk ->
            chk
        }

        checkerManager.value = false


        checkerManager.addSource(validTermsCheck, { _ -> validationCheck()})

        btnEnable = Transformations.map(checkerManager) {
            checkerManager.value
        }
    }

    private fun validationCheck(){
        val ch1 = validTermsCheck.value

        checkNotNull(ch1) {return}

        if(ch1){
            //Pass
            Timber.d("PASS")
            checkerManager.value = true
        }else {
            checkerManager.value = false
        }
    }
}