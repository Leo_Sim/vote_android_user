package com.incuvoteu.ui.login.viewmodel

import androidx.lifecycle.*
import com.incuvoteu.api.client.SignApiClient
import com.incuvoteu.util.ToastUtil
import com.incuvoteu.util.ValidUtil
import timber.log.Timber

class SignUpFragmentStep1ViewModel : ViewModel() {

    var emailText: MutableLiveData<String> =
        MutableLiveData<String>().apply { postValue("leo@incublock.io") }
    var validEmail: LiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }
    var pwdText: MutableLiveData<String> =
        MutableLiveData<String>().apply { postValue("Zxcv#1234") }
    var validPwd: LiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }
    var rePwdText: MutableLiveData<String> =
        MutableLiveData<String>().apply { postValue("Zxcv#1234") }
    var validRePwd: LiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }
    var emailCheck: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }
    var checkerManager: MediatorLiveData<Boolean> = MediatorLiveData()

    var btnEnable: LiveData<Boolean>

    init {

        validEmail = Transformations.map(emailText) { email ->
            emailCheck.value = false
            var res = false
            if (ValidUtil.isValidEmail(email)) {
                res = true
            }
            res
        }


        validPwd = Transformations.map(pwdText) { pwd ->
            var res = false
            rePwdText.value = rePwdText.value
            if (ValidUtil.isValidPwd(pwd)) {
                res = true
            }
            res
        }

        validRePwd = Transformations.map(rePwdText) { rePwd ->
            var res = false
            val pwd = pwdText.value!!
            if (rePwd == pwd && ValidUtil.isValidPwd(rePwd)) {
                res = true
            }
            res
        }

        checkerManager.value = false


        checkerManager.addSource(validEmail, { f -> validationCheck() })
        checkerManager.addSource(validPwd, { f -> validationCheck() })
        checkerManager.addSource(validRePwd, { f -> validationCheck() })
        checkerManager.addSource(emailCheck, { f -> validationCheck() })

        btnEnable = Transformations.map(checkerManager) {
            checkerManager.value
        }
    }

    private fun validationCheck() {
        val ch1 = validEmail.value
        Timber.d("ch1 : %s ", ch1)
        val ch2 = validPwd.value
        Timber.d("ch2 : %s ", ch2)
        val ch3 = validRePwd.value
        Timber.d("ch3 : %s ", ch3)
        val ch4 = emailCheck.value

        checkNotNull(ch1) { return }
        checkNotNull(ch2) { return }
        checkNotNull(ch3) { return }
        checkNotNull(ch4) { return }

        if (ch1 && ch2 && ch3 && ch4) {
            //Pass
            Timber.d("PASS")
            checkerManager.value = true
        } else {
            checkerManager.value = false
        }
    }

    fun emailCheck() {
        emailText.value.let {
            SignApiClient.emailCheck(it!!).subscribe(
                {
                    emailCheck.value = true
                    ToastUtil.make("사용 가능한 계정입니다.")
                }, { err -> ToastUtil.make(err.message) })
        }
    }
}