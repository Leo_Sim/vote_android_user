package com.incuvoteu.ui.login.viewmodel

import androidx.lifecycle.*
import com.incuvoteu.util.ValidUtil
import timber.log.Timber

class SignUpFragmentStep2ViewModel : ViewModel() {

    var phoneText : MutableLiveData<String> = MutableLiveData<String>().apply { postValue("01012345678")}
    var validPhone : LiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }

    var checkerManager : MediatorLiveData<Boolean> = MediatorLiveData()

    var btnEnable : LiveData<Boolean>

    init{

        validPhone = Transformations.map(phoneText) {n ->
            var res = false
            if(ValidUtil.isValidPhone(n)){
                res = true
            }
            res
        }

        checkerManager.value = false


        checkerManager.addSource(validPhone, { f -> validationCheck()})

        btnEnable = Transformations.map(checkerManager) {
            checkerManager.value
        }
    }

    private fun validationCheck(){
        val ch1 = validPhone.value

        checkNotNull(ch1) {return}

        if(ch1){
            //Pass
            Timber.d("PASS")
            checkerManager.value = true
        }else {
            checkerManager.value = false
        }
    }
}