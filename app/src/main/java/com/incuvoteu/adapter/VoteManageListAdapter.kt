package com.incuvoteu.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.incuvoteu.R
import com.incuvoteu.adapter.base.BaseRecyclerAdapter
import com.incuvoteu.databinding.VoteOptionListItemBinding
import com.incuvoteu.ui.main.viewmodel.VoteOptionViewModel
import com.incuvoteu.viewholder.BaseViewHolder
import com.incuvoteu.viewholder.VoteManageAddVH
import com.incuvoteu.viewholder.VoteManageItemVH

class VoteManageListAdapter(
    val parentViewmodel: ViewModel,
    val lifecycle: LifecycleOwner
) : BaseRecyclerAdapter() {

    //NEED LIVEDATA
    companion object {
        val TYPE_ITEM = 0
        val TYPE_ADD = 1
    }

    fun setItem(_items: List<Any>) {
        items.clear()
        items.addAll(_items)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size + 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            TYPE_ITEM -> {
                val binding = DataBindingUtil.inflate<VoteOptionListItemBinding>(
                    inflater,
                    R.layout.vote_option_list_item,
                    parent,
                    false
                )
                val viewmodel = VoteOptionViewModel()
                binding.viewmodel = viewmodel
                val holder = VoteManageItemVH(parentViewmodel,binding)
                binding.lifecycleOwner = lifecycle
                viewmodel.saveMediator.observe(lifecycle, Observer<Unit> {
                    //do nothing
                    Log.d("devsim", "saved")
                })
                return holder
            }
            TYPE_ADD -> return VoteManageAddVH(
                inflateView(
                    parent,
                    R.layout.vote_manage_list_item_add
                ), parentViewmodel
            )
        }
        val binding = DataBindingUtil.inflate<VoteOptionListItemBinding>(
            inflater,
            R.layout.vote_option_list_item,
            parent,
            false
        )
        return VoteManageItemVH(parentViewmodel,binding)
    }

    override fun getItemViewType(position: Int): Int {
        if (position == items.size) {
            return TYPE_ADD
        }
        return TYPE_ITEM
    }


}