package com.incuvoteu.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.incuvoteu.R
import com.incuvoteu.viewholder.BaseViewHolder
import com.leo.kakaocompleterecyclerview.viewholder.*

class CompleteResultRecyclerAdapter : CompleteRecyclerAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when(viewType){
            TYPE_FIRST_LOADING -> {
                return LoadingViewHolder(inflater.inflate(R.layout.loading_list_item,parent,false))
            }
            TYPE_EMPTY -> {
                return EmptyViewHolder(inflater.inflate(R.layout.empty_list_item,parent,false))
            }
            TYPE_MORE -> {
                return MoreViewHolder(inflater.inflate(R.layout.more_list_item,parent,false))
            }
            else -> {
                val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.main_home_vote_list_item,parent,false)
                CompleteMainHomeItemViewHolder(binding)
            }
        }
    }
}