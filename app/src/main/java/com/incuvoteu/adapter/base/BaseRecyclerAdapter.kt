package com.incuvoteu.adapter.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.incuvoteu.viewholder.BaseViewHolder
import kotlin.reflect.KClass

open class BaseRecyclerAdapter : RecyclerView.Adapter<BaseViewHolder> {
    /**
     * 옵션. 싱글 타입 리스트인 경우 사용
     */
    @LayoutRes
    protected var layout: Int = 0  // 레이아웃 리소스
    protected lateinit var VH: KClass<out BaseViewHolder> // VH KClass

    var items = ArrayList<Any>()

    fun addItems(_items: List<Any>) {
        items.addAll(_items)
        notifyDataSetChanged()
    }

    constructor()
    constructor(@LayoutRes _layout: Int, _VH: KClass<out BaseViewHolder>) {
        this.layout = _layout
        this.VH = _VH
    }

    fun inflateView(parent: ViewGroup, layout_res: Int) =
        LayoutInflater.from(parent.context).inflate(layout_res, parent, false)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        val cs = VH.java.getConstructor(View::class.java)
        return cs.newInstance(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if (position < items.size) {
            holder.bind(items[position])
        }
    }

    fun getBindingVH(parent: ViewGroup, layout : Int, VH_KClass : KClass<out BaseViewHolder>) : BaseViewHolder{
        val inflater = LayoutInflater.from(parent.context)
        val binding : ViewDataBinding = DataBindingUtil.inflate(
            inflater,
            layout,
            parent,
            false
        )
        val cs = VH_KClass.java.getConstructor(ViewDataBinding::class.java)
        return cs.newInstance(binding)
    }
}