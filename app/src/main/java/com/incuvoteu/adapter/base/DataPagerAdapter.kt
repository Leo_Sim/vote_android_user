package com.incuvoteu.adapter.base

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import kotlin.reflect.KClass

class DataPagerAdapter : FragmentPagerAdapter {

    constructor(
        fm: FragmentManager,
        src: List<Triple<String,KClass<out Fragment>,Parcelable?>>
    ) : super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        this.pages = src
    }

    private var pages: List<Triple<String,KClass<out Fragment>,Parcelable?>>

    override fun getItem(position: Int): Fragment {
        if(pages[position].third != null) {
            var bundle = Bundle()
            bundle.putParcelable(pages[position].first, pages[position].third)
            val ff = pages[position].second.java.newInstance()
            ff.arguments = bundle
            return ff
        }
        return pages[position].second.java.newInstance()
    }

    override fun getCount(): Int = pages.size
}