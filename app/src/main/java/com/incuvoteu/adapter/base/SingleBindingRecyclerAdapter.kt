package com.incuvoteu.adapter.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.incuvoteu.viewholder.BaseViewHolder
import kotlin.reflect.KClass

open class SingleBindingRecyclerAdapter : BaseRecyclerAdapter {

    constructor(@LayoutRes _layout: Int, _VH: KClass<out BaseViewHolder>) :super(_layout,_VH){
        this.layout = _layout
        this.VH = _VH
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding : ViewDataBinding = DataBindingUtil.inflate(
            inflater,
            layout,
            parent,
            false
        )
        val cs = VH.java.getConstructor(ViewDataBinding::class.java)
        return cs.newInstance(binding)
    }

}