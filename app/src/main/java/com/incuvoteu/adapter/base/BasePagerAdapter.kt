package com.incuvoteu.adapter.base

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import kotlin.reflect.KClass

class BasePagerAdapter : FragmentPagerAdapter {

    constructor(
        fm: FragmentManager,
        src: List<Pair<String,KClass<out Fragment>>>
    ) : super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        this.pages = src
    }

    private var pages: List<Pair<String,KClass<out Fragment>>>

    override fun getItem(position: Int): Fragment {
        return pages[position].second.java.newInstance()
    }

    override fun getCount(): Int = pages.size
}