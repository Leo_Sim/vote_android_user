package com.incuvoteu.adapter

import android.view.ViewGroup
import com.incuvoteu.R
import com.incuvoteu.adapter.base.BaseRecyclerAdapter
import com.incuvoteu.viewholder.BaseViewHolder
import com.incuvoteu.viewholder.VoteHeaderVH
import com.incuvoteu.viewholder.VoteViewHolder

class MainVoteListAdapter : BaseRecyclerAdapter() {
    companion object {
        val TYPE_HEADER = 0
        val TYPE_ITEM = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        when(viewType){
            TYPE_HEADER -> return getBindingVH(parent, R.layout.vote_header_list_item, VoteHeaderVH::class)
            else -> return getBindingVH(parent, R.layout.main_home_vote_list_item, VoteViewHolder::class)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if(position == 0) return TYPE_HEADER
        return TYPE_ITEM
    }
}