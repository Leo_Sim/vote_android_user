package com.incuvoteu.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.incuvoteu.R
import com.incuvoteu.viewholder.BaseViewHolder
import com.leo.kakaocompleterecyclerview.viewholder.EmptyViewHolder
import com.leo.kakaocompleterecyclerview.viewholder.LoadingViewHolder
import com.leo.kakaocompleterecyclerview.viewholder.MainHomeItemViewHolder
import com.leo.kakaocompleterecyclerview.viewholder.MoreViewHolder

open class CompleteRecyclerAdapter : RecyclerView.Adapter<BaseViewHolder>() {
    companion object {
        val TYPE_FIRST_LOADING = 0
        val TYPE_ITEM = 1
        val TYPE_EMPTY = 2
        val TYPE_MORE = 3
    }
    var isFirstLoading = true
    var lastPage = false

    var items = mutableListOf<Any>()

    fun addItems(_items : List<Any>){
        checkNotNull(_items){return}
        items.addAll(_items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when(viewType){
            TYPE_FIRST_LOADING -> {
                return LoadingViewHolder(inflater.inflate(R.layout.loading_list_item,parent,false))
            }
            TYPE_EMPTY -> {
                return EmptyViewHolder(inflater.inflate(R.layout.empty_list_item,parent,false))
            }
            TYPE_MORE -> {
                return MoreViewHolder(inflater.inflate(R.layout.more_list_item,parent,false))
            }
            else -> {
                val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.main_home_vote_list_item,parent,false)
                MainHomeItemViewHolder(binding)
            }
        }
    }

    fun refresh(){
        isFirstLoading = true
        lastPage = false
        items.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        if(isFirstLoading){ //처음로딩바
            return 1
        }
        if(!isFirstLoading && items.size == 0){ //처음로딩 지남, 아이템 받아왔는데 없을 때
            return 1
        }
        if(lastPage) return items.size //마지막 페이지일때 more 버튼 없애고 순수하게 아이템만 보여줌

        return items.size + 1 //more 가능한 경우 more 버튼 붙여줌
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if(position < items.size) {
            holder.bind(items[position])
        }
    }

    override fun getItemViewType(position: Int): Int {
        if(isFirstLoading) {
            isFirstLoading = false
            return TYPE_FIRST_LOADING
        }
        if(!isFirstLoading && items.size == 0){
            return TYPE_EMPTY
        }
        if(position == items.size){
            return TYPE_MORE
        }
        return TYPE_ITEM
    }
}