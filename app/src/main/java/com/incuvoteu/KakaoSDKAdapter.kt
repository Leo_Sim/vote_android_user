package com.incuvoteu

import com.kakao.auth.*

class KakaoSDKAdapter : KakaoAdapter() {
    override fun getApplicationConfig(): IApplicationConfig {
        return IApplicationConfig { GlobalApplication.getGlobalApplicationContext() }
    }

    override fun getSessionConfig(): ISessionConfig {
        return object : ISessionConfig {
            override fun isSaveFormData(): Boolean = true

            override fun getAuthTypes(): Array<AuthType> = arrayOf(AuthType.KAKAO_LOGIN_ALL)

            override fun isSecureMode(): Boolean = true

            override fun getApprovalType(): ApprovalType? = ApprovalType.INDIVIDUAL

            override fun isUsingWebviewTimer(): Boolean = false
        }
    }

    override fun getPushConfig(): IPushConfig {
        return super.getPushConfig()
    }
}