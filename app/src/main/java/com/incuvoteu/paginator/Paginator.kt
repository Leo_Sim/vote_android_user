package com.incuvoteu.paginator

import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.incuvoteu.adapter.CompleteRecyclerAdapter
import com.incuvoteu.api.FetchState

class Paginator(
    val rec: RecyclerView,
    val loadMore: (Int, Int) -> Unit,
    val fetchState: () -> FetchState
) : RecyclerView.OnScrollListener() {
    var page: Int = 1
    var size: Int = 20
    var threshold = 5

    var layoutManager: LinearLayoutManager

    init {
        rec.addOnScrollListener(this)
        layoutManager = rec.layoutManager as LinearLayoutManager
    }

    fun refresh(){
        fetchState().isOnLast = false
        fetchState().isOnLoading = false
        fetchState().isOnError = false
        page = 1
        val adapter = rec.adapter as CompleteRecyclerAdapter
        adapter.refresh()
        Log.d("devsim","refresh")
        loadMore(page++, size)
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
        if (fetchState().isOnLoading || fetchState().isOnLast) return

        if (visibleItemCount + lastVisibleItemPosition + threshold >= totalItemCount) {
            Log.d("devsim","scrolled0")
            loadMore(page++, size)
        }
    }
}