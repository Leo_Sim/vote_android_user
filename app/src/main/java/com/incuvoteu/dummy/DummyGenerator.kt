package com.incuvoteu.dummy

import com.incuvoteu.model.VoteListItem
import com.incuvoteu.model.VoteResultItem
import kotlin.random.Random

object DummyGenerator {

    fun createVoteItem(size: Int): List<VoteListItem> {
        val arrays = arrayListOf<VoteListItem>()
        for (i in 0 until size) {
            arrays.add(
                VoteListItem(
                    "KBS 입맞춤",
                    "SBS MTV '더쇼' 사전투표 진행 중!!!",
                    "2020.02.18",
                    "2020.02.20",
                    3729,
                    255,
                    "https://www.mbcsportsplus.com/data/board/attach/2019/06/20190627103013_tduczzok.png"
                )
            )
        }
        return arrays
    }

    fun createVoteResultItem(size : Int) : List<VoteResultItem> {
        val arrays = mutableListOf<VoteResultItem>()
        for (i in 0 until size) {
            val ratio = String.format("%.2f", Random.nextDouble(10.0, 100.0)).toFloat().toFloat()
            arrays.add(
                VoteResultItem(
                    "FIESTA",
                    "ONE",
                    "https://www.mbcsportsplus.com/data/board/attach/2019/06/20190627103013_tduczzok.png",
                    ratio
                )
            )
        }
        return arrays.sortedWith(Comparator<VoteResultItem> { o1, o2 -> (o2.ratio!! - o1.ratio!!).toInt() })
    }
}