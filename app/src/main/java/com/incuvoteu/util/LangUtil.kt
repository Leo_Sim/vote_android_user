package com.incuvoteu.util

import android.content.Context
import java.util.*

object LangUtil {
    /**
     * 해당 Locale에 맞는 리소스 스트링 반환
     *
     * @param context
     * @param locale
     * @param resourceId
     * @return
     */
    fun getLocaleResourceString(
        context: Context,
        locale: Locale,
        resourceId: Int
    ): String? {
        val resources = context.resources
        val conf = resources.configuration
        conf.locale = locale
        resources.updateConfiguration(conf, null)
        return resources.getString(resourceId)
    }
}