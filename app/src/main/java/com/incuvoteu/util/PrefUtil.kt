package com.incuvoteu.util

import androidx.preference.PreferenceManager
import com.incuvoteu.GlobalApplication

object PrefUtil {
    /**
     * Preference Key
     */

    //Auth
    val AUTH_TYPE = "auth_type"
    val AUTH_EMAIL_TOKEN = "auth_email_token"
    val AUTH_KAKAO_TOKEN = "auth_kakao_token"
    //Lang
    val LANG_LOCALE = "lang_locale"

    private val sp = PreferenceManager.getDefaultSharedPreferences(GlobalApplication.getGlobalApplicationContext())

    fun set(key: String, value: Any?) {
        val editor = sp.edit()
        when(value){
            is String -> editor.putString(key,value)
            is Boolean -> editor.putBoolean(key, value)
            is Int -> editor.putInt(key, value)
            else -> {
                //Exception
            }
        }
        editor.apply()
    }

    fun get(key: String, def: Int): Int = sp.getInt(key, def)
    fun get(key: String, def: String = "default"): String = sp.getString(key, def)!!
    fun get(key: String, def: Boolean): Boolean = sp.getBoolean(key, def)
}