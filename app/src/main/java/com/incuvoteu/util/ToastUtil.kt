package com.incuvoteu.util

import android.widget.Toast
import com.incuvoteu.GlobalApplication

object ToastUtil {
    fun make(msg : String?){
        Toast.makeText(GlobalApplication.getGlobalApplicationContext(),msg,Toast.LENGTH_LONG).show()
    }
}