package com.incuvoteu.util

import java.util.regex.Pattern

object ValidUtil {
    /**
     * validation check
     * @param val
     * @param regex
     * @return boolean
     */
    fun isValid(`val`: String?, regex: String?): Boolean {
        var result = false
        val p = Pattern.compile(regex)
        val m = p.matcher(`val`)
        if (m.matches()) {
            result = true
        }
        return result
    }

    /**
     * 이메일 유효성 체크
     * @param email
     * @return boolean
     */
    fun isValidEmail(email: String?): Boolean = isValid(
        email,
        "^[0-9a-zA-Z]([-_\\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\\.]?[0-9a-zA-Z])*\\.[a-zA-Z]{2,3}$"
    )

    /**
     * 패스워드 유효성 체크
     * @param pwd
     * @return boolean
     */
    fun isValidPwd(pwd: String?): Boolean {
        //대소문자 숫자 조합 8자리 이상
        //return isValid(pwd, "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,32}$")
        //대소문자 특수문자 숫자 조합 8자리 이상
        return isValid(pwd, "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[~#?!@$%^&*-]).{8,32}$");
    }

    /**
     * 휴대전화 번호 유효성 체크
     * @param phone
     * @return boolean
     */
    fun isValidPhone(phone: String?): Boolean {
        return isValid(phone, "^01([0|1|6|7|8|9]?)?([0-9]{3,4})?([0-9]{4})$")
    }

    /**
     * 닉네임 유효성 체크
     * @param nick
     * @return boolean
     */
    fun isValidNick(nick: String?): Boolean {
        return isValid(nick, "^[가-힣a-zA-Z0-9]{2,8}$")
    }
}