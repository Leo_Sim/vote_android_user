package com.incuvoteu

import android.app.Application
import com.incuvoteu.manager.LangManager
import com.kakao.auth.KakaoSDK
import timber.log.Timber
import timber.log.Timber.DebugTree


class GlobalApplication : Application() {
    companion object {
        private lateinit var instance : GlobalApplication

        fun getGlobalApplicationContext() : GlobalApplication {
            checkNotNull(instance) { "this application does not inherit com.kakao.GlobalApplication" }
            return instance
        }
    }


    override fun onCreate() {
        super.onCreate()
        instance = this

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        } else {
            //do nothing
        }
        setLanguage()
        KakaoSDK.init(KakaoSDKAdapter())
    }

    private fun setLanguage() = LangManager.applyLanguage(LangManager.locale)
}