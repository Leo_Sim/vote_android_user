package com.incuvoteu

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.incuvoteu.manager.NetworkManager

open class BaseActivity : AppCompatActivity() {
    protected lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.context = this
    }

    override fun onResume() {
        super.onResume()
        setNetworkStateReceiver()
    }

    private fun setNetworkStateReceiver() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            NetworkManager.registerNetworkStateReceiver(
                this,
                NetworkManager.NetworkStateManager(),
                object : NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        super.onAvailable(network)
                        NetworkManager.setConnectedState(true)
                        Toast.makeText(context, "켜짐", Toast.LENGTH_SHORT).show()
                    }

                    override fun onLost(network: Network) {
                        super.onLost(network)
                        NetworkManager.setConnectedState(false)
                        Toast.makeText(context, "꺼짐", Toast.LENGTH_SHORT).show()
                    }
                })
        } else {
            NetworkManager.registerNetworkStateReceiver(
                this,
                NetworkManager.NetworkStateManager(),
                object : BroadcastReceiver() {
                    override fun onReceive(
                        context: Context,
                        intent: Intent
                    ) {
                        NetworkManager.isConnected(context)
                        Toast.makeText(context, "켜짐", Toast.LENGTH_SHORT).show()
                    }
                })
        }
    }
}